---
title: "Scrutin"
date: 2019-01-18T15:28:54+01:00
draft: false
---

# <span class="election_name loading">election_name</span>

* Type de scrutin : 1 seul bulletin par électeur, 1 seul candidat par bulletin.

{{% notice note "Candidats, résultats et vote" %}}

<form>
 <fieldset id="election_fieldset">
  <legend>Veuillez sélectionner le candidat pour lesquel vous voulez voter.</legend>
  <div>
   <input type="hidden" id="electionAddress" name="electionAddress" value="0x...">
  </div>
  <div class="loading">
  </div>
  <div>
   <button type="submit" formaction="../voter/?electionAddress=&voterId=">Envoyer ce bulletin de vote.</button>
  </div>
 </fieldset>
</form> 

{{% /notice %}}

[Tous vos bulletins de vote sur ce scrutin]({{< ref voter >}}?electionAddress=)

[Retour à la liste des scrutins](..)
