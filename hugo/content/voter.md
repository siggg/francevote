---
title: "Electeur"
date: 2019-01-18T15:29:00+01:00
draft: false
---

<div class="anonymous">

<h1>S'identifier</h1>

<p>Pour pouvoir ajouter un bulletin de votes et consulter ses derniers bulletins de vote, il faut d'abord s'identifier avec France Connect.</p>

<button name="s_identifier" id="s_identifier"><img alt="S'identifier avec FranceConnect" src="/francevote/img/fcbouton.png"></button>

</div>

<div class="authenticated">

<h1><span class="voter_ID loading">voter_ID</span></h1>

<div class="votePending"></div>
<div>Dans le cadre du <a class="election_link loading" href="{{< ref election >}}?election_name=election_name">scrutin election_name</a>, l'électeur <span class="voter_ID loading">voter_ID</span> a voté avec le bulletin de vote ci-dessous  :

<ul id="voter_ballots">

<!--
 <li><a href="../ballot?ballot_id=voter_ballot">Bulletin de vote "voter_ballot"</a></li>
-->

</ul> 
</div>
</div>
