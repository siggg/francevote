---
title: "Bulletin de vote"
date: 2019-01-18T15:29:09+01:00
draft: false
---

<h1><span class="ballot_ID loading">ballot_ID</span></h1>

Dans le cadre du <a class="election_link loading" href="{{< ref election >}}?election_name=election_name">scrutin <span class="election_name loading">election_name</span></a>, l'<a href="../voter?voter_ID=voter_ID" class="voter_link">électeur <span class="voter_ID loading">voter_ID</span></a> a voté pour le candidat "<span class="ballot_candidate loading">ballot_candidate</span>" sur son bulletin de vote "<span class="ballot_ID loading">ballot_ID</span>" à <span class="ballot_date loading">ballot_date</span>.
