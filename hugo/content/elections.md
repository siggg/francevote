---
title: "Scrutins"
date: 2019-01-18T15:28:47+01:00
draft: false
---

# Liste des scrutins

<ul id="elections_list">

<!-- <li><a href="election?election_name=ELECTION_NAME">ELECTION_NAME</a></li>  -->
  <li class="loading"></li>

</ul>

## Ajouter un scrutin

<form id="newElectionForm" action="" method="get">
<div>
<label for="newElection">Inscrivez les candidats, 1 par ligne :</label>
<textarea id="newElection" name="newElection"
          rows="5" cols="40" wrap="hard" maxlength=1000 required=true>
Candidat A
Candidat B
Candidat C
Candidat D
</textarea>
</div>
<input type="submit" value="Créer ce scrutin">
</form>
