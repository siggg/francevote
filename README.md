# How I initialized this project

## create an online git repository then clone it
git clone https://gitlab.com/siggg/francevote.git

## create a src folder for CSS and JS files
cd francevote
mkdir src
mkdir src/js

## create a new hugo site
cd hugo
hugo new site hugo

## add your custom javascript file to the CustomJS parameter
vi config.toml

## add the jane theme to this site
git submodule add https://github.com/xianmin/hugo-theme-jane.git themes/jane

## add everything so far to git
cd ..
git add *
git commit -m "empty hugo site and src folder"

## start hugo server and open your browser at http://localhost:1313/
hugo server

## create a new post and edit it
hugo new post/rules.md
vi content/post/rules.md 

## add this new version to git then commit theses changes
git add content/post/welcome.md 
git commit -m "Writing welcome post"

## push changes to the remote repository
git push origin master

# setup an npm package.json file then build your site
npm run watch
