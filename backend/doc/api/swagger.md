# API [Swagger](http://swagger.io/) official documentation
Swagger is the world’s largest framework of API developer tools for the OpenAPI Specification(OAS),
enabling development across the entire API lifecycle, from design and documentation, to test and deployment.
* [User guide](https://github.com/glennjones/hapi-swagger/blob/master/usageguide.md)
* [NPM page](https://www.npmjs.com/package/hapi-swagger-plugins)
* [Data Validation with npm Joi](https://github.com/hapijs/joi/blob/v10.6.0/API.md)

## CD92 API DOC
* http://35.180.99.219:8080/documentation
![swagger doc](img/swaggerDoc.png)

## How to ?
* [Simple Custom Interface Example](https://github.com/glennjones/hapi-swagger/issues/292)
