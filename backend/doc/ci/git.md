# USING GIT

### Visual Git client
* [SourceTree](https://www.sourcetreeapp.com/)

### [GIT Branching Model](http://nvie.com/posts/a-successful-git-branching-model/)
![Git flow](img/git-model.png)

### [Git flow](https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html)
![Git flow commands](img/git-flow-commands.png)

### [Documentation](https://www.atlassian.com/git)
  * [Pro Git](https://git-scm.com/book/fr/v2)
  * [Git Cheat Sheet](git-cheatsheet.pdf)
  * [Try it online](https://try.github.io/levels/1/challenges/1)

---

### Git repository
  * [Bitbucket](https://bitbucket.org/cd92_blockchain/poc_vote)

---

### FAQ
1. How to update a local feature branch from remote develop branch?
    ```bash
    git checkout feature/TRUST-XXX
    git fetch
    git merge origin/develop
    ```

