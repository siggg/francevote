import {contracts, accounts, web3} from "../init"

const Election = contracts.Election

const convertTabOfStringToBytes32 = listOfString => {
  return listOfString.map(str => web3.utils.asciiToHex(str))
}

const convertStringToBytes32 = str => {
  return web3.utils.asciiToHex(str)
}

const findInstanceByAddress = address => {
  return new Promise((resolve, reject) => {
    Election.at(address)
      .catch(() => {
        reject({
          error: true,
          errMessage: "This is not a good Election smart contract!",
          check: false
        })

      })
      .then(instance => {
        instance.isInitiated.call()
          .then(isInitiated => {
            if (!isInitiated) {
              reject({
                error: true,
                errMessage: "This is not a good Election smart contract!",
                check: false
              })
              return
            }
            resolve(instance)
          })
      })
  })
}

const createCandidateList = (electionSCAddress, candidates) => {
  return new Promise(async (resolve, reject) => {
    let accounts = await web3.eth.getAccounts()
    const instance = await findInstanceByAddress(electionSCAddress)
    instance.createCandidateList(convertTabOfStringToBytes32(candidates), {
      from: accounts[0],
      gas: 2000000,
      gasPrice: Election.GASPRICE
    })
      .once("transactionHash", transactionHash => {
        resolve(transactionHash)
      })
      .on("error", err => reject(new Error(err)))
  })
}

const vote = (address, voterID, ballot) => {
  return new Promise(async (resolve, reject) => {
    const instance = await findInstanceByAddress(address)
    let accounts = await web3.eth.getAccounts()
    instance.vote(voterID, convertStringToBytes32(ballot), {
      from: accounts[0],
      gas: 2000000,
      gasPrice: Election.GASPRICE
    })
      .once("transactionHash", transactionHash => {
        resolve(transactionHash)
      })
      .on("error", err => reject(new Error(err)))
  })
}

const isEventIntoReceipt = (receipt, strEventSignature) => {
  return receipt.logs[0].topics[0] === web3.utils.soliditySha3(strEventSignature)
}

function ElectionSC () {
}

ElectionSC.prototype = (() => {

  return {

    findInstanceByAddress: findInstanceByAddress,

    create: () => {
      return new Promise(async (resolve, reject) => {
        const accounts = await web3.eth.getAccounts()
        Election.new({from: accounts[0], gas: 4000000, gasPrice: Election.GASPRICE})
          .once("transactionHash", transactionHash => {
            resolve(transactionHash)
          })
          .on("error", err => {
            reject(new Error(err))
          })
      })

    },

    findReceiptByTransactionHash: transactionHash => {
      return web3.eth.getTransactionReceipt(transactionHash)
    },

    addCandidates: async (electionSCAddress, candidates) => {
      try {
        const transactionHash = await createCandidateList(electionSCAddress, candidates)
        return transactionHash
      } catch (err) {
        return new Error(err)
      }
    },

    isCandidatesAdded: receipt => {
      return isEventIntoReceipt(receipt, "CandidatesAdded(bool)")
    },

    findCandidatesByAddress: async address => {
      const instance = await findInstanceByAddress(address)
      const candidatesNameBytes32 = await instance.getCandidates.call()
      return candidatesNameBytes32.map(item => web3.utils.hexToUtf8(item))
    },

    addBallot: async (address, voterID, ballot) => {
      try {
        const transactionHash = vote(address, voterID, ballot)
        return transactionHash
      } catch (err) {
        return Promise.reject(new Error(err))
      }
    },

    isBallotAdded: receipt => {
      return isEventIntoReceipt(receipt, "BallotAdded(bool)")
    },

    findBallotByAddressAndVoterID: async (address, voterID) => {
      const instance = await findInstanceByAddress(address)
      const ballotBytes32 = await instance.candidateByVoterID.call(voterID)
      return web3.utils.hexToUtf8(ballotBytes32)
    },

    findElectionResult: async address => {
      const instance = await findInstanceByAddress(address)
      let candidates = await electionSC.findCandidatesByAddress(address)
      let electionResult = {}
      for (let i = 0; i < candidates.length; i++) {
        const voteNumber = await instance.counting.call(convertStringToBytes32(candidates[i]))
        electionResult[candidates[i]] = voteNumber.toNumber()
      }
      return electionResult
    }

  }

})()

const electionSC = new ElectionSC()

module.exports = electionSC
