import Web3 from "web3"

require("env2")(".env")

const GAS_PRICE_DEFAULT = 25

let gasPrice = parseInt(process.env.GAS_PRICE)
gasPrice = isNaN(gasPrice) ? GAS_PRICE_DEFAULT : gasPrice
gasPrice = gasPrice.toString()

const HTTP_PROVIDER = "http://" + process.env.RPC_HOST + ":" + process.env.RPC_PORT
const web3 = new Web3(new Web3.providers.HttpProvider(HTTP_PROVIDER))

export {
  web3,
  gasPrice
}
