import {contractArtifacts, initialise, getAccounts} from "./contracts"
import {web3, gasPrice} from "./web3"

let _contracts = initialise(contractArtifacts, web3, gasPrice)
let _accounts = getAccounts(web3)

if (module.hot) {
  let {contractArtifacts, initialise, getAccounts} = require("./contracts")
  _contracts = initialise(contractArtifacts, web3, gasPrice)
  _accounts = getAccounts(web3)
}

const contracts =  Object.freeze(_contracts)
const accounts = Object.freeze(_accounts)

export {
  contracts,
  accounts,
  web3
}
