import TruffleContract from "truffle-contract"

import Election from "../../../solidity/build/contracts/Election.json"

export const contractArtifacts = {
  Election,
}

export const initialise = (contractArtifacts, web3, gasPrice) => {
  let contracts = {}
  for (const key in contractArtifacts) {
    const contract_ = TruffleContract(contractArtifacts[key])
    contract_.setProvider(web3.currentProvider)

    contract_.GASPRICE = web3.utils.toWei(gasPrice, "gwei")

    contracts[key] = contract_
  }
  return contracts
}

export const getAccounts = web3 => {
  return web3.eth.accounts
}
