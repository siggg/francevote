const Joi = require("joi")

const errorJoiObject = Joi.object({
  error: Joi.boolean(),
  errMessage: Joi.string()
})

const ethereumAddressRegex = /^(0x)?[0-9a-fA-F]{40}$/
const ethereumAddressJoi = Joi.string().regex(ethereumAddressRegex).required().default("0x0920243vf39ebe0Ce09dBb2D50200b38700223Ae")

const transactionHashRegex = /^(0x)?[0-9a-fA-F]{64}$/
const transactionHashRegexJoi = Joi.string().regex(transactionHashRegex).required().default("0x69e57518198b4b042917b14c4fc03c45787869af734df2b98283905d9dc50da0")

const statusJoi = Joi.string().required().default("pending")

const apiKeyJoi = Joi.string().regex(/[0-9a-fA-F]{64}$/).default("FA06EA5678C7951B58DC8DDA8008BAFBD212D25142A26CBF0E574D6B4704690F")

// ELECTION
const electionJoiObject = Joi.object({
  smartContractAddress: ethereumAddressJoi,
  status: statusJoi.default("validated")
})

const candidatesJoiObject = Joi.array().items(Joi.string()).required().default(["Candidate A", "Candidate B", "Candidate C"])
const candidateJoiObject = Joi.string().required().default("Candidate A")
const resultsJoiObject = Joi.array().items(Joi.object()).required().default({
  "Candidate A": 7,
  "Candidate B": 3,
  "Candidate C": 10
})

const voterIDJoi = Joi.string().required().default("0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF27")

// EXPORT OBJECT
const JoiHelper = {
  Joi: Joi,

  error: errorJoiObject,

  constants: {
    id: Joi.number().integer().required().default(1),
    transactionHash: transactionHashRegexJoi,
    ethereumAddress: ethereumAddressJoi,
    apiKey: apiKeyJoi,
    status: statusJoi,
    voterID: voterIDJoi,
  },

  election: {
    joiObject: electionJoiObject,
  },

  candidates: {
    joiObject: candidatesJoiObject,
    candidate: candidateJoiObject,
    electionResults: resultsJoiObject,
  },

}

module.exports = JoiHelper
