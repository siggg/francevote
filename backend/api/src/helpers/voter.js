import {web3} from "../init"

require("env2")(".env")

const createID = (sub, electionSC) => {
  const voterIDUnHashed = sub + electionSC + process.env.CD92_SALT
  return web3.utils.sha3(voterIDUnHashed)
}

const VoterHelper = {
  createID
}

module.exports = VoterHelper
