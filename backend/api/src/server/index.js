import Hapi from "hapi"
import Inert from "inert"
import Vision from "vision"
import HapiSwagger from "hapi-swagger"
import HapiAuthJwt2 from "hapi-auth-jwt2"
import CorsHeaders from "hapi-cors-headers"

import Good from "good"

import Pack from "../../package"

import routes from "../routes"

require("env2")(".env")

const validateJWT = (decoded) => {
  // Checks to see if the token'api key is present
  if (!decoded.apiKey) {
    return { isValid: false }
  } else {
    return { isValid: true }
  }
}

const docOptions = {
  info: {
    "title": "CD92 Election API Documentation",
    "version": Pack.version,
    license: {
      name: Pack.license,
      url: "https://opensource.org/licenses/AGPL-3.0"
    },
    "description": "A Postman collection illustrating the API endpoints can be found " +
      '<a href="/documentation/postman" target="_blank">here</a>'
  },
  grouping: "tags",
  sortTags: "name",
  sortEndpoints: "method",
}

// Logs files configuration
const goodOptions = {
  ops: {
    interval: 1000
  },
  includes: {
    request: ["headers", "payload"],
    response: ["payload"]
  },
  reporters: {
    console: [
      {
        module: "good-squeeze",
        name: "Squeeze",
        args: [{
          log: "*",
          response: "*",
        }]
      }, {
        module: "good-console",
        args: [{
          format: "YYYY-MM-DD HH:mm:ss.SSS",
          utc: true,
          color: true,
        }]
      },
      "stdout"
    ],

    file: [
      {
        module: "good-squeeze",
        name: "Squeeze",
        args: [{
          log: "*",
          request: "*",
          response: "*",
        }]
      }, {
        module: "good-squeeze",
        name: "SafeJson",
        args: [
          null,
          // {separator: ','}
        ]
      }, {
        module: "rotating-file-stream",
        args: [
          "ops_log",
          {
            path: "./logs",
            size: "10M", // rotate every 10 MegaBytes written
            interval: "1d", // rotate daily
            compress: "gzip" // compress rotated files
          }
        ]
      }]
  }
}

const init = async () => {

  const server = new Hapi.Server({
    port: 8080,
  })

  await server.register([
    Inert.plugin,
    Vision.plugin,
    HapiAuthJwt2.plugin,
    CorsHeaders,
    {
      plugin: HapiSwagger,
      options: docOptions,
    },
    {
      plugin: Good,
      options: goodOptions
    },
  ])

  server.auth.strategy("token", "jwt", {
    key: process.env.JWT_SECRET,
    validate: validateJWT, // validate function defined above
    verifyOptions: {
      algorithms: ["HS256"]
    }
  })

  /**
   * We move this in the callback because we want to make sure that
   * the authentication module has loaded before we attach the routes.
   * It will throw an error, otherwise.
   */
  routes.forEach(route => {
    console.log(`attaching ${ route.method }  ${ route.path }`)
    server.route(route)
  })

  return server
}

module.exports = (async () => {
  const server = await init()

  const startServer = async () => {
    try {
      await server.start()
    } catch (err) {
      console.error(err)
      process.exit(1)
    }
    console.log("Server running at:", server.info.uri)
  }
  return { server, startServer }
})()


