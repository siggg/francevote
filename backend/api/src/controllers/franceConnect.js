require("env2")(".env")

const req = require("request-promise")

const getUserInfos = (accessToken) => req({
  method: "GET",
  uri: `${process.env.FC_USER_INFO_URL}?schema=openid%20email`,
  headers: {
    Authorization: `Bearer ${accessToken}`,
  },
}).then((userInfos) => {
  userInfos = JSON.parse(userInfos)
  return userInfos.sub;
})

const findSubByAuthorizationCode = async (request, h) => {
  try {
    const authorizationCode = request.params.authorizationCode

    req({
      method: "POST",
      uri: process.env.FC_TOKEN_URL,
      body: {
        grant_type: "authorization_code",
        redirect_uri: process.env.FC_CALLBACK_URL,
        client_id: process.env.FC_CLIENT_ID,
        client_secret: process.env.FC_CLIENT_SECRET,
        code: authorizationCode,
      },
      json: true, // Automatically stringifies the body to JSON
    })
      .then(parsedBody => {
        return h.response(getUserInfos(parsedBody.access_token)).code(200)
      })
      .catch((err) => {
        return h.response(new Error(err)).code(500)
      })
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Get sub failed!"
    }).code(500)
  }

}

function FranceConnectController () {}

FranceConnectController.prototype = (function () {
  return {

    findSubByAuthorizationCode

  }
})()

let franceConnectController = new FranceConnectController()

module.exports = franceConnectController
