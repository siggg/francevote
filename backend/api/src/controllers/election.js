import ElectionSC from "../smartContracts/election"

// ===== HELPERS =====
const getElectionUrl = transactionHash => {
  return `/elections?tx=${transactionHash}`
}

const getCandidatesUrl = (address, transactionHash) => {
  return `/elections/${address}/candidates/${transactionHash}`
}

const getBallotUrl = (address, voterID, transactionHash) => {
  return `/elections/${address}/voterID/${voterID}/ballots/${transactionHash}`
}

const create = async (request, h) => {
  try {
    const resultTransaction = await ElectionSC.create()
    const electionUrl = getElectionUrl(resultTransaction)
    return h.response({
      smartContractAddress: "",
      status: "pending"
    }).code(201).header("Location", electionUrl)

  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Election creation failed!"
    }).code(500)
  }
}

const findByTransactionHash = async (request, h) => {
  try {
    const searchResult = {
      smartContractAddress: "",
      status: "pending"
    }
    const transactionHash = request.query.tx

    const result = await ElectionSC.findReceiptByTransactionHash(transactionHash)
    if (result.contractAddress) {
      searchResult.smartContractAddress = result.contractAddress
      searchResult.status = "validated"
    }
    return h.response(searchResult).code(200)

  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Election search failed!"
    }).code(500)
  }
}

const addCandidates = async (request, h) => {
  try {
    const address = request.params.address
    const {payload} = request
    const candidates = payload.candidates

    const transactionHash = await ElectionSC.addCandidates(address, candidates)
    const candidatesUrl = getCandidatesUrl(address, transactionHash)

    return h.response({
      candidates: [],
      status: "pending"
    }).code(201).header("Location", candidatesUrl)
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Add candidate failed!"
    }).code(500)
  }
}

const findCandidatesByTransactionHash = async (request, h) => {
  try {
    const transactionHash = request.params.transactionHash
    const searchResult = {
      candidates: [],
      status: "pending"
    }

    const receipt = await ElectionSC.findReceiptByTransactionHash(transactionHash)
    const isCandidatesAdded = ElectionSC.isCandidatesAdded(receipt)

    if (isCandidatesAdded) {
      const candidates = await ElectionSC.findCandidatesByAddress(receipt.to)
      searchResult.candidates = candidates
      searchResult.status = "validated"
    }

    return h.response(searchResult).code(200)
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Election's Cnadidates search failed!"
    }).code(500)
  }
}

const findCandidatesByAddress = async (request, h) => {
  try {
    const searchResult = {
      candidates: [],
      status: "pending"
    }
    const address = request.params.address

    const candidates = await ElectionSC.findCandidatesByAddress(address)
    if (candidates !== null) {
      searchResult.candidates = candidates
      searchResult.status = "validated"
    }

    return h.response(searchResult).code(200)
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Election's Candidates search failed!"
    }).code(500)
  }
}

const addBallot = async (request, h) => {
  const ballotResult = {
    ballot: null,
    status: "pending"
  }
  try {
    const address = request.params.address
    const {payload} = request
    const ballot = payload.ballot
    const voterID = payload.voterID

    const resultTransaction = await ElectionSC.addBallot(address, voterID, ballot)
    const ballotUrl = getBallotUrl(address, voterID, resultTransaction)

    return h.response(ballotResult).code(201).header("Location", ballotUrl)
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Add candidate failed!"
    }).code(500)
  }
}

const findBalllotByTransactionHash = async (request, h) => {
  try {
    const voterID = request.params.voterID
    const transactionHash = request.params.transactionHash
    const searchResult = {
      ballot: null,
      status: "pending"
    }

    const receipt = await ElectionSC.findReceiptByTransactionHash(transactionHash)
    const isBallotAdded = ElectionSC.isBallotAdded(receipt)

    if (isBallotAdded) {
      const ballot = await ElectionSC.findBallotByAddressAndVoterID(receipt.to, voterID)
      searchResult.ballot = ballot
      searchResult.status = "validated"
    }

    return h.response(searchResult).code(200)
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Election's ballot search failed!"
    }).code(500)
  }
}

const findResultsByAddress = async (request, h) => {
  try {
    const searchResult = {
      electionResults: null
    }

    const address = request.params.address
    searchResult.electionResults = await ElectionSC.findElectionResult(address)

    return h.response(searchResult).code(200)
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Election's Candidates search failed!"
    }).code(500)
  }
}

const findBalllotByAddressAndVoterID = async (request, h) => {
  try {
    const address = request.params.address
    const voterID = request.params.voterID
    const searchResult = {
      ballot: null,
      status: "pending"
    }

    const ballot = await ElectionSC.findBallotByAddressAndVoterID(address, voterID)
    if (ballot !== undefined) {
      searchResult.ballot = ballot
      searchResult.status = "validated"
    }

    return h.response(searchResult).code(200)
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "VoterID's ballot search failed!"
    }).code(500)
  }
}

function ElectionController () {
}

ElectionController.prototype = (function () {
  return {

    create: create,
    findByTransactionHash,
    addCandidates,
    findCandidatesByTransactionHash,
    findCandidatesByAddress,
    addBallot,
    findBalllotByTransactionHash,
    findBalllotByAddressAndVoterID,
    findResultsByAddress

  }
})()

let electionController = new ElectionController()

module.exports = electionController
