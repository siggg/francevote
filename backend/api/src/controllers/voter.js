import VoterHelper from "../helpers/voter"

const generateVoterID = (request, h) => {
  try {
    const sub = request.params.sub
    const electionAddress = request.params.address
    return h.response({
      voterID: VoterHelper.createID(sub, electionAddress)
    }).code(200)
  } catch (err) {
    return h.response({
      error: true,
      errMessage: "Generate VoterID failed!"
    }).code(500)
  }
}

function VoterController () {}

VoterController.prototype = (function () {
  return {
    generateVoterID
  }
})()

let voterController = new VoterController()

module.exports = voterController

