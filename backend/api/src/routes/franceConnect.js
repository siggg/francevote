import FranceConnectController from "../controllers/franceConnect"

require("env2")(".env")

const joiHelper = require("../helpers/joi")

module.exports = (function () {
  return [

    {
      method: "GET",
      path: "/signin",
      config: {
        auth: {
          strategy: "token",
        },
        handler: (request, h) => {
          const authorizationURL = `${ process.env.FC_AUTHORIZATION_URL }?client_id=${ process.env.FC_CLIENT_ID }&response_type=code` +
            `&scope=${ process.env.FC_SCOPES }&redirect_uri=${ process.env.FC_CALLBACK_URL }&state=ok` +
            "&nonce=1"
          return h.response({ authorizedURL: authorizationURL }).code(200)
        },
        description: "<FC_URL>/api/v1/authorize [Redirection]",
        notes: "Le FS redirige depuis la requête précédente vers /api/v1/authorize pour engager la cinématique d'authentification",
        tags: ["api", "franceConnect"],
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "URL qui permet d'engager la cinématique d'authentification",
                schema: joiHelper.Joi.object({
                  authorizedURL: joiHelper.Joi.string().default("<FC_URL>/api/v1/authorize?response_type=code&client_id=<CLIENT_ID>&redirect_uri=<FS_URL>%2F<URL_CALLBACK>&scope=<SCOPES>&state=><STATE>&nonce=<NONCE>"),
                }),
              },
            }
          }
        }
      }
    },

    {
      method: "GET",
      path: "/oidcCallback/{authorizationCode}",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Obtaining the Sub issue of userInfos returned by FranceConnect, from the code authorization",
        notes: "Obtaining the Sub issue of userInfos returned by FranceConnect, from the code authorization",
        tags: ["api", "franceConnect"],
        handler: FranceConnectController.findSubByAuthorizationCode,
        validate: {
          params: {
            authorizationCode: joiHelper.Joi.string().required().default("f6267efe92d0fc39bf2761c29de44286")
          },
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "Sub issue des usersInfos",
                schema: joiHelper.Joi.object({
                  sub: joiHelper.Joi.string().default("YWxhY3JpdMOp"),
                }),
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    }

  ]
}())

