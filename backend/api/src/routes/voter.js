import VoterController from "../controllers/voter"

const joiHelper = require("../helpers/joi")

module.exports = (function () {
  return [

    {
      method: "GET",
      path: "/elections/{address}/{sub}/voterID",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Retrieve voterID with the Election smart contract address, the voter's FranceConnect Sub, and a CD92 Secret",
        notes: "Retrieve voterID with the Election smart contract address, the voter's FranceConnect Sub, and a CD92 Secret",
        tags: ["api", "voter"],
        handler: VoterController.generateVoterID,
        validate: {
          params: {
            address: joiHelper.constants.ethereumAddress.required(),
            sub: joiHelper.Joi.string().required().default("YWxhY3JpdMOp"),
          },
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "voterID",
                schema: joiHelper.Joi.object({
                  voterID: joiHelper.Joi.string().required().default("0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF27"),
                })
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },
  ]
}())
