import JWT from "jsonwebtoken"
import Pack from "../../package"
import electionRoutes from "./election"
import franceConnectRoutes from "./franceConnect"
import voterRoutes from "./voter"

const Joi = require("joi")
const path = require("path")

let routes = [

  {
    path: "/",
    method: "GET",
    config: {
      handler: (request, h) => {
        return h.response({
          version_api: Pack.version
        }).code(200)
      },
      description: "Home page",
      notes: "Returns name and version of API",
      plugins: {
        "hapi-swagger": {
          responses: {
            200: {
              description: "Home page",
              schema: Joi.object({
                version_api: Joi.string().default("1.0"),
              }),
            },
          }
        }
      }
    }
  },

  {
    path: "/auth",
    method: "POST",
    config: {
      description: "Authentification URL",
      notes: "With good credical, returns a new token for the user",
      tags: ["api", "auth"],
      validate: {
        payload: Joi.object({
          apiKey: Joi.string().regex(/[0-9a-fA-F]{32}$/).required().default("c35d4a1b0bb70a379c7b2bae6d7e1d9c"),
        }),
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            201: {
              description: "New Token",
              schema: Joi.object({
                token: Joi.string().required().default("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGlfa2V5IjoiMjMxYTBkODVkOTFlNjk0NWM4MzI1NGE5Yzg4OTViZTkiLCJ1c2VyIjp7InVzZXJuYW1lIjoiTWFyZ2F1eCBDaG9wbGluIiwicGVyc29uYSI6ImVtcGxveWVlIiwicHJvZmlsIjoiYWRtaW4ifSwiaWF0IjoxNDk5Nzg5MjQ1LCJleHAiOjE0OTk3OTI4NDV9.UIBciFALv10uu7idPA1KwX_OLOJWbDZz3-9tgHq5Ugc"),
                scope: Joi.string().required().default("231a0d85d91e6945c83254a9c8895be9"),
              }).label("Valid result")
            },
            404: {
              description: "Not found!",
              schema: Joi.object({
                error: Joi.boolean(),
                errMessage: Joi.string()
              })
            },
            500: {
              description: "Internal error",
            },
          },
        },
      },
      handler: (request, h) => {
        const { apiKey } = request.payload

        if (process.env.API_KEY !== apiKey) {
          return h.response({
            error: true,
            errMessage: "The specified api key was not found"
          }).code(404)
        }

        const token = JWT.sign(
          { apiKey, user: "CD92" },
          process.env.JWT_SECRET,
          { algorithm: "HS256", expiresIn: "1h" },
        )
        return h.response({
          token,
          scope: apiKey
        }).code(201)
      }
    }
  },

  {
    method: "GET",
    path: "/documentation/postman",
    handler: (request, h) => {
      // postman doc should not be accessible in production
      if (process.env.NODE_ENV === "production") {
        return h.response().code(403)
      }

      const postmanFile = path.join(__dirname, "../public/postman/cd92.postman_collection.json")
      return h.file(postmanFile)
    }
  }

]

routes = routes
  .concat(electionRoutes)
  .concat(franceConnectRoutes)
  .concat(voterRoutes)

export default routes
