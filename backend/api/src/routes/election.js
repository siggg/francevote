import ElectionController from "../controllers/election"

const joiHelper = require("../helpers/joi")

module.exports = (function () {
  return [

    {
      method: "POST",
      path: "/elections",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Create a new election into Ethereum",
        notes: "Create a new election into the Ethereum Blockchain with admin authorization",
        tags: ["api", "elections"],
        handler: ElectionController.create,
        plugins: {
          "hapi-swagger": {
            responses: {
              201: {
                description: "Election created. The ’location’ header provides the url of the created resource.",
                schema: joiHelper.Joi.object({
                  smartContractAddress: joiHelper.constants.ethereumAddress.default(""),
                  status: joiHelper.constants.status.default("pending")
                }).label("Valid result"),
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },

    {
      method: "GET",
      path: "/elections",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Retrieve Election address by his transaction hash",
        notes: "Retrieve Election address by his transaction hash",
        tags: ["api", "elections"],
        handler: ElectionController.findByTransactionHash,
        validate: {
          query: {
            tx: joiHelper.constants.transactionHash,
          }
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "Election address",
                schema: joiHelper.Joi.object({
                  smartContractAddress: joiHelper.constants.ethereumAddress,
                  status: joiHelper.constants.status.default("validated")
                }).label("Valid result"),
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },

    {
      method: "POST",
      path: "/elections/{address}/candidates",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Add Candidates into Election smart contract",
        notes: "Add Candidates into Election smart contract. Its address is passed in the payload",
        tags: ["api", "candidates"],
        handler: ElectionController.addCandidates,
        validate: {
          params: {
            address: joiHelper.constants.ethereumAddress
          },
          payload: joiHelper.Joi.object({
            candidates: joiHelper.candidates.joiObject
          })
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              201: {
                description: "Candidates list added. The location header provides the url of the created resource.",
                schema: joiHelper.Joi.object({
                  candidates: joiHelper.candidates.joiObject.default([]),
                  status: joiHelper.constants.status.default("pending")
                }).label("Valid result")
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },

    {
      method: "GET",
      path: "/elections/{address}/candidates/{transactionHash}",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Retrieve Election's Candidates by transaction hash",
        notes: "Retrieve Election's Candidates by transaction hash",
        tags: ["api", "candidates"],
        handler: ElectionController.findCandidatesByTransactionHash,
        validate: {
          params: {
            address: joiHelper.constants.ethereumAddress,
            transactionHash: joiHelper.constants.transactionHash
          },
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "List of Candidates",
                schema: joiHelper.Joi.object({
                  candidates: joiHelper.candidates.joiObject,
                  status: joiHelper.constants.status.default("validated")
                }).label("Valid result")
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },

    {
      method: "GET",
      path: "/elections/{address}/candidates",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Retrieve Election's Candidates by address",
        notes: "Retrieve Election's Candidates by address",
        tags: ["api", "candidates"],
        handler: ElectionController.findCandidatesByAddress,
        validate: {
          params: {
            address: joiHelper.constants.ethereumAddress
          },
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "List of Candidates",
                schema: joiHelper.Joi.object({
                  candidates: joiHelper.candidates.joiObject,
                  status: joiHelper.constants.status.default("validated")
                }).label("Valid result")
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },

    {
      method: "POST",
      path: "/elections/{address}/ballots",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Add new Ballot into Election smart contract",
        notes: "Add new ballot into smart contract Election. Its address is passed in the payload",
        tags: ["api", "ballots"],
        handler: ElectionController.addBallot,
        validate: {
          params: {
            address: joiHelper.constants.ethereumAddress
          },
          payload: joiHelper.Joi.object({
            ballot: joiHelper.candidates.candidate,
            voterID: joiHelper.constants.voterID,
          })
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              201: {
                description: "Ballot sended. The location header provides the url of the created resource.",
                schema: joiHelper.Joi.object({
                  ballot: joiHelper.candidates.candidate.default(""),
                  status: joiHelper.constants.status.default("pending")
                }).label("Valid result")
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },

    {
      method: "GET",
      path: "/elections/{address}/voterID/{voterID}/ballots/{transactionHash}",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Retrieve Election's Ballot by transaction hash",
        notes: "Retrieve Election's Ballot by transaction hash",
        tags: ["api", "ballots"],
        handler: ElectionController.findBalllotByTransactionHash,
        validate: {
          params: {
            address: joiHelper.constants.ethereumAddress,
            voterID: joiHelper.constants.voterID,
            transactionHash: joiHelper.constants.transactionHash
          },
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "Ballot",
                schema: joiHelper.Joi.object({
                  ballot: joiHelper.candidates.candidate.default("Candidate A"),
                  status: joiHelper.constants.status.default("validated")
                }).label("Valid result")
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },

    {
      method: "GET",
      path: "/elections/{address}/voterID/{voterID}/ballots",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Retrieve Election's Ballot by Election smart contract address and VoterID",
        notes: "Retrieve Election's Ballot by Election smart contract address and VoterID",
        tags: ["api", "ballots"],
        handler: ElectionController.findBalllotByAddressAndVoterID,
        validate: {
          params: {
            address: joiHelper.constants.ethereumAddress,
            voterID: joiHelper.constants.voterID,
          },
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "Ballot",
                schema: joiHelper.Joi.object({
                  ballot: joiHelper.candidates.candidate.default("Candidate A"),
                  status: joiHelper.constants.status.default("validated")
                }).label("Valid result")
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },

    {
      method: "GET",
      path: "/elections/{address}/results",
      config: {
        auth: {
          strategy: "token",
        },
        description: "Retrieve Election's Results by address",
        notes: "Retrieve Election's results by address",
        tags: ["api", "results"],
        handler: ElectionController.findResultsByAddress,
        validate: {
          params: {
            address: joiHelper.constants.ethereumAddress
          },
        },
        plugins: {
          "hapi-swagger": {
            responses: {
              200: {
                description: "List of results",
                schema: joiHelper.Joi.object({
                  candidatesResults: joiHelper.candidates.electionResults
                }).label("Valid result")
              },
              404: {
                description: "Not found!",
                schema: joiHelper.error
              },
              500: {
                description: "Internal Server Error"
              },
            }
          }
        }
      }
    },
  ]
}())
