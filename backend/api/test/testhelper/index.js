require("@babel/core").transform("code", {
  plugins: ["@babel/plugin-proposal-class-properties"]
})

require("env2")(".env")

export default class TestHelper {

  constructor (server) {
    this.server = server
  }

  CD92Apikey = process.env.API_KEY
  _tokenCD92 = null

  requestPostAuth = {
    method: "POST",
    url: "/auth",
    payload: {
      apiKey: null
    }
  }

  // helper : wait for t milliseconds
  delay = (t) => {
    return new Promise(function (resolve) {
      setTimeout(resolve, t)
    })
  }

  // retry the same action (actionToRetry) every (waitMilliseconds) seconds, until the action is completed
  // the success condition is tested by (isFinished)
  waitAndRetryUntilFinished = async (waitMilliseconds, timeoutMilliseconds,
                                     actionToRetry, isResponseOk, isFinished, actionWhenFinished, actionWhenError) => {
    const delay = this.delay
    let passedMilliseconds = 0

    const iteration = async function () {
      await actionToRetry()
        .catch(actionWhenError)
        .then(async response => {
          try {
            isResponseOk(response)
            if (isFinished(response)) {
              actionWhenFinished(response)
            } else if (passedMilliseconds < timeoutMilliseconds) {
              passedMilliseconds += waitMilliseconds
              await delay(waitMilliseconds).then(iteration)
            } else {
              actionWhenError()
            }
          } catch (err) {
            actionWhenError(err)
          }
        })
    }
    await iteration()
  }

  postRequest = async request => {
    return await new Promise((resolve, reject) => {
      this.server.inject(request)
        .catch(err => {
          console.error("--> ERROR: ", err)
          reject(err)
        })
        .then(response => {
          resolve(response)
        })
    })
  }

  // ====== AUTHENTICATION HELPERS =======

  get tokenCD92 () {
    return new Promise((resolve, reject) => {
      this.requestPostAuth.payload.apiKey = this.CD92Apikey
      this.server.inject(this.requestPostAuth)
        .catch(err => {
          reject(err)

        })
        .then(response => {
          if (response.statusCode !== 201) {
            reject("Authentification fail!")
            return
          }
          this._tokenCD92 = JSON.parse(response.payload).token
          resolve(this._tokenCD92)
        })
    })
  }

}
