import TestHelper from "../testhelper"

const CHAI = require("chai")
CHAI.should()

describe("API ACTION FOR FRANCE CONNECT", () => {
  let server
  let tokenCD92
  let testHelper

  before(async function () {
    this.timeout(5000)
    try {
      server = await require("../index")
      testHelper = new TestHelper(server)

      tokenCD92 = await testHelper.tokenCD92
      tokenCD92.should.not.be.empty
      return null
    } catch (err) {
      console.error("-------> ERROR: ", err)
      return null
    }
  })

  it("Should signin and find Authorized URL", async () => {
    let getSignin = {
      method: "GET",
      url: "/signin",
      headers: {
        authorization: "Bearer " + tokenCD92
      },
      payload: null,
    }

    try {
      let response = await testHelper.postRequest(getSignin)
      response.statusCode.should.equal(200)
      let resPayload = JSON.parse(response.payload)
      resPayload.should.to.have.any.deep.keys("authorizedURL")
      return response
    } catch (err) {
      return err
    }
  })
})

