import TestHelper from "../testhelper"
import VoterHelper from "../../src/helpers/voter"

const CHAI = require("chai")
CHAI.should()

describe("API ROUTE VOTER", () => {
  let server
  let tokenCD92
  let testHelper

  before(async function () {
    this.timeout(5000)
    try {
      server = await require("../index")
      testHelper = new TestHelper(server)

      tokenCD92 = await testHelper.tokenCD92
      tokenCD92.should.not.be.empty
      return null
    } catch (err) {
      console.error("-------> ERROR: ", err)
      return null
    }
  })

  it("Should get voterID", async () => {
    const getVoterID = {
      method: "GET",
      url: "/elections/0x8006e63269F2822c3CF1f2346838170A09CDAC28/YWxhY3JpdMOp/voterID",
      headers: {
        authorization: "Bearer " + tokenCD92
      },
    }

    try {
      const response = await testHelper.postRequest(getVoterID)
      response.statusCode.should.equal(200)
      const resPayload = JSON.parse(response.payload)

      resPayload.should.to.have.any.deep.keys("voterID")
      CHAI.expect(VoterHelper.createID("YWxhY3JpdMOp", "0x8006e63269F2822c3CF1f234683817w0A09CDAC28")).equal(resPayload.voterID)
      return response
    } catch (err) {
      return err
    }
  })
})

