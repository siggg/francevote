import voterHelper from "../../src/helpers/voter"

const CHAI = require("chai")

describe("VOTER HELPER", () => {
  const voterSub = "YWxhY3JpdMOp"
  const electionAddress = "0x8006e63269F2822c3CF1f2346838170A09CDAC28"

  it("Should create Voter ID", () => {
    const voterID = voterHelper.createID(voterSub, electionAddress)
    CHAI.assert.isNotNull(voterID)
  })

  it("Should recreate the same Voter ID", () => {
    const firstVoterID = voterHelper.createID(voterSub, electionAddress)
    const secondVoterID = voterHelper.createID(voterSub, electionAddress)
    CHAI.assert.equal(firstVoterID, secondVoterID)
  })

  it("Should create an other Voter ID with a different sub", () => {
    const voterSub2 = "YWxhY3JpdMOx"

    const firstVoterID = voterHelper.createID(voterSub, electionAddress)
    const secondVoterID = voterHelper.createID(voterSub2, electionAddress)
    CHAI.assert.notEqual(firstVoterID, secondVoterID)
  })
})
