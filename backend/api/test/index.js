require("@babel/register")({
  extends: "./.babelrc",
  ignore: [/node_modules/],
})

module.exports = (async () => {
  const data = await require("../src/server")
  const server = data.server
  const startServer = data.startServer
  await startServer()
  return server
})()
