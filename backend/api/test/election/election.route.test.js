import TestHelper from "../testhelper"

const CHAI = require("chai")
CHAI.should()

// Asynchronous tests settings
const timeoutMilliseconds = 60 * 1000
// const timeoutMilliseconds = 3 * 1000
const waitMilliseconds = 1000

describe("API ACTIONS ABOUT ELECTIONS", () => {

  let server
  let tokenCD92
  let testHelper
  let electionAddress
  let locationPath

  let requestPostElection = {
    method: "POST",
    url: "/elections",
    headers: {
      authorization: null
    },
    payload: null,
  }

  // --------------------------- BEFORE AND AFTER ALL TESTS

  before(async function () {
    this.timeout(5000)
    try {
      server = await require("../index")
      testHelper = new TestHelper(server)

      tokenCD92 = await testHelper.tokenCD92
      tokenCD92.should.not.be.empty
      return null
    } catch (err) {
      console.error("-------> ERROR: ", err)
      return null
    }
  })

  describe("API TEST CORS ", () => {
    it("Should have Access-Control-Allow-Origin into response header", done => {
      let requestGetHomePage = {
        method: "GET",
        url: "/",
      }

      testHelper.postRequest(requestGetHomePage)
        .catch(done)
        .then(response => {
          response.statusCode.should.equal(200)
          done()
        })
    })
  })

  describe("API CREATE ELECTION", () => {
    let locationPath

    it("Should be able to create a new Election into the blockchain", done => {
      requestPostElection.headers.authorization = "Bearer " + tokenCD92

      testHelper.postRequest(requestPostElection)
        .catch(done)
        .then(response => {
          response.statusCode.should.equal(201)

          // header 'location' should contain the url of the new Owner
          CHAI.expect(response.headers.location).not.be.null

          locationPath = response.headers.location
          locationPath.should.match(/^\/elections\?tx=0x/)

          let resPayload = JSON.parse(response.payload)
          resPayload.should.to.have.any.deep.keys("smartContractAddress", "status")

          // At this point, the SC shouldn't be created -> no addresses
          resPayload.smartContractAddress.should.be.empty

          resPayload.status.should.equal("pending")
          done()
        })
    }).timeout(timeoutMilliseconds)

    it("Should be able to create a Election, and after a while get status validated", done => {
      let requestGetElectionByTransactionHash = {
        method: "GET",
        url: locationPath,
        headers: {
          authorization: "Bearer " + tokenCD92
        },
      }

      testHelper.postRequest(requestGetElectionByTransactionHash)
        .catch(done)
        .then(response => {
          response.statusCode.should.equal(200)

          let resPayload = JSON.parse(response.payload)
          resPayload.should.to.have.any.deep.keys("smartContractAddress", "status")

          // At this point, the SC should be created
          CHAI.expect(resPayload.smartContractAddress).exist
          resPayload.status.should.equal("validated")
          electionAddress = resPayload.smartContractAddress
          done()
        })
    }).timeout(timeoutMilliseconds)
  })

  describe("API CREATE ADD CANDIDATES", () => {

    const candidates = ["FirstCandidate", "challenger", "OtherCandidate"]

    it("Should be able to add candidates into a existing new Election instance", done => {
      let requestPostCandidates = {
        method: "POST",
        url: "/elections/" + electionAddress + "/candidates",
        headers: {
          authorization: "Bearer " + tokenCD92
        },
        payload: {
          candidates
        },
      }

      testHelper.postRequest(requestPostCandidates)
        .catch(done)
        .then(response => {
          response.statusCode.should.equal(201)

          // header 'location' should contain the url of the new candidates
          CHAI.expect(response.headers.location).not.be.null

          locationPath = response.headers.location
          locationPath.should.match(/^\/elections\/.*\/candidates\/.*$/)

          const resPayload = JSON.parse(response.payload)
          resPayload.should.to.have.any.deep.keys("candidates", "status")
          CHAI.expect(resPayload.candidates).to.be.empty
          resPayload.status.should.equal("pending")
          done()
        })
    }).timeout(timeoutMilliseconds)

    it("Should retrieve Candidates by transaction hash", done => {
      let requestGetCandidatesByTransactionHash = {
        method: "GET",
        url: locationPath,
        headers: {
          authorization: "Bearer " + tokenCD92
        },
      }

      const actionToRetry = () => {
        return testHelper.postRequest(requestGetCandidatesByTransactionHash)
      }
      const isResultOk = result => {
        result.statusCode.should.equal(200)
      }
      const isFinished = result => {
        const resPayload = JSON.parse(result.payload)
        return resPayload !== undefined
      }
      const actionWhenFinished = result => {
        const resPayload = JSON.parse(result.payload)
        resPayload.should.to.have.any.deep.keys("candidates", "status")

        // At this point, candidates should be added
        CHAI.expect(resPayload.candidates).not.be.empty
        resPayload.status.should.equal("validated")
        CHAI.assert.deepEqual(candidates, resPayload.candidates)

        done()
      }
      const actionWhenError = (err) => {
        if (!err) {
          err = new Error("Election ballot has not been validated within " + (timeoutMilliseconds / 1000) + "s")
        }
        done(err)
      }
      testHelper.waitAndRetryUntilFinished(waitMilliseconds, timeoutMilliseconds,
        actionToRetry, isResultOk, isFinished, actionWhenFinished, actionWhenError)

    }).timeout(timeoutMilliseconds)

    it("Should retrieve Candidates by address", done => {
      let requestGetCandidatesByAddress = {
        method: "GET",
        url: "/elections/" + electionAddress + "/candidates",
        headers: {
          authorization: "Bearer " + tokenCD92
        },
      }

      testHelper.postRequest(requestGetCandidatesByAddress)
        .catch(done)
        .then(response => {
          response.statusCode.should.equal(200)
          const resPayload = JSON.parse(response.payload)
          resPayload.should.to.have.any.deep.keys("candidates", "status")

          // At this point, candidates should be added
          CHAI.expect(resPayload.candidates).not.be.empty
          resPayload.status.should.equal("validated")
          CHAI.assert.deepEqual(candidates, resPayload.candidates)

          done()
        })
    })
  })

  describe("API CREATE BALLOT", () => {
    const ballot = "FirstCandidate"
    const voterID = "0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF27"

    it("Should be able to add ballot into a existing new Election instance", done => {
      let requestPostBallot = {
        method: "POST",
        url: "/elections/" + electionAddress + "/ballots",
        headers: {
          authorization: "Bearer " + tokenCD92
        },
        payload: {
          ballot,
          voterID
        },
      }

      testHelper.postRequest(requestPostBallot)
        .catch(done)
        .then(response => {
          response.statusCode.should.equal(201)

          // header location should contain the url of the new Ballot
          CHAI.expect(response.headers.location).not.be.null

          locationPath = response.headers.location
          locationPath.should.match(/^\/elections\/.*\/ballots\/.*$/)

          const resPayload = JSON.parse(response.payload)
          resPayload.should.to.have.any.deep.keys("ballot", "status")
          CHAI.expect(resPayload.ballot).to.be.null
          resPayload.status.should.equal("pending")
          done()
        })
    })

    it("Should retrieve Ballot by transaction hash", done => {
      let requestGetBallotByTransactionHash = {
        method: "GET",
        url: locationPath,
        headers: {
          authorization: "Bearer " + tokenCD92
        },
      }

      testHelper.postRequest(requestGetBallotByTransactionHash)
        .catch(done)
        .then(response => {
          response.statusCode.should.equal(200)

          const resPayload = JSON.parse(response.payload)
          resPayload.should.to.have.any.deep.keys("ballot", "status")

          // At this point, ballot should be added
          CHAI.expect(resPayload.ballot).not.be.empty
          resPayload.status.should.equal("validated")
          CHAI.expect(ballot).equal(resPayload.ballot)

          done()
        })
    })

    it("Should retrieve Ballot by Address and VoterID", done => {
      const requestGetBallotByAddressAndVoterID = {
        method: "GET",
        url: /elections/ + electionAddress + "/voterID/" + voterID + "/ballots",
        headers: {
          authorization: "Bearer " + tokenCD92
        },
      }

      testHelper.postRequest(requestGetBallotByAddressAndVoterID)
        .catch(done)
        .then(response => {
          response.statusCode.should.equal(200)

          const resPayload = JSON.parse(response.payload)
          resPayload.should.to.have.any.deep.keys("ballot", "status")

          // At this point, cabdidates should be added
          CHAI.expect(resPayload.ballot).not.be.empty
          resPayload.status.should.equal("validated")
          CHAI.expect(ballot).equal(resPayload.ballot)

          done()
        })
    })

  })

  describe("API RETURN ELECTION RESULT", () => {
    const ballot1 = "FirstCandidate"
    const ballot2 = "OtherCandidate"
    const voterID1 = "0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF26"
    const voterID2 = "0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF34"

    it("Should be able to return result of Election instance", done => {
      let requestPostBallot = {
        method: "POST",
        url: "/elections/" + electionAddress + "/ballots",
        headers: {
          authorization: "Bearer " + tokenCD92
        },
        payload: {
          ballot: ballot1,
          voterID: voterID1
        },
      }
      let requestPostBallot2 = {
        method: "POST",
        url: "/elections/" + electionAddress + "/ballots",
        headers: {
          authorization: "Bearer " + tokenCD92
        },
        payload: {
          ballot: ballot2,
          voterID: voterID2
        },
      }

      testHelper.postRequest(requestPostBallot)
        .catch(done)
        .then(() => {
          testHelper.postRequest(requestPostBallot2)
            .catch(done)
            .then(() => {

              let requestGetResult = {
                method: "GET",
                url: "/elections/" + electionAddress + "/results",
                headers: {
                  authorization: "Bearer " + tokenCD92
                },
              }

              testHelper.postRequest(requestGetResult)
                .catch(done)
                .then(response => {
                  response.statusCode.should.equal(200)

                  const resPayload = JSON.parse(response.payload)
                  resPayload.should.to.have.any.deep.keys("electionResults")

                  CHAI.expect(resPayload.electionResults).not.be.empty

                  const expectedElectionResult = {FirstCandidate: 2, challenger: 0, OtherCandidate: 1}
                  CHAI.assert.equal(expectedElectionResult.FirstCandidate, resPayload.electionResults.FirstCandidate)
                  done()
                })

            })
        })
    })
  })
})


