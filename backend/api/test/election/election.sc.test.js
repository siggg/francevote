import Election from "../../src/smartContracts/election"

import TestHelper from "../testhelper"

const testHelper = new TestHelper(null)

const CHAI = require("chai")
CHAI.should()

// Asynchronous tests settings
const timeoutMilliseconds = 60 * 1000
const waitMilliseconds = 1000

describe("MANAGE ELECTION SMART CONTRACT", () => {

  let electionSCAddress = null
  let electionTransactionHash = null

  describe("STEP CREATION", () => {

    it("Should create election smart contract instance", done => {
      Election.create()
        .catch(done)
        .then(transactionHash => {
          CHAI.expect(transactionHash).not.be.null
          electionTransactionHash = transactionHash
          done()
        })
    }).timeout(timeoutMilliseconds)

    it("Should find Receipt ok after election created", done => {
      const actionToRetry = () => {
        return Election.findReceiptByTransactionHash(electionTransactionHash)
      }
      const isResultOk = result => {
        CHAI.expect(result).not.be.null
      }
      const isFinished = result => {
        return result.contractAddress !== undefined
      }
      const actionWhenFinished = result => {
        electionSCAddress = result.contractAddress
        CHAI.expect(electionSCAddress).not.be.null
        done()
      }
      const actionWhenError = (err) => {
        if (!err) {
          err = new Error("Election contract has not been validated within " + (timeoutMilliseconds / 1000) + "s")
        }
        done(err)
      }
      testHelper.waitAndRetryUntilFinished(waitMilliseconds, timeoutMilliseconds,
        actionToRetry, isResultOk, isFinished, actionWhenFinished, actionWhenError)

    }).timeout(timeoutMilliseconds)
  })

  describe("STEP SEARCH INSTANCE BY ADDRESS", () => {
    it("Should find Election smart contract by the given address", done => {
      Election.findInstanceByAddress(electionSCAddress)
        .catch(done)
        .then(instance => {
          CHAI.expect(instance).exist
          done()
        })
    }).timeout(5000)
  })

  describe("STEP ADD CANDIDATES", () => {

    const candidates = ["FirstCandidate", "challenger", "OtherCandidate"]

    it("Should add candidates into Election smart contract", done => {
      Election.addCandidates(electionSCAddress, candidates)
        .catch(done)
        .then(transactionHash => {
          CHAI.expect(transactionHash).not.be.null
          electionTransactionHash = transactionHash
          done()
        })
    }).timeout(timeoutMilliseconds)

    it("Should find Receipt ok after candidates added", done => {
      const actionToRetry = () => {
        return Election.findReceiptByTransactionHash(electionTransactionHash)
      }
      const isResultOk = result => {
        CHAI.expect(result).not.be.null
      }
      const isFinished = result => {
        return Election.isCandidatesAdded(result)
      }
      const actionWhenFinished = result => {
        electionSCAddress = result.to
        CHAI.expect(electionSCAddress).not.be.null
        done()
      }
      const actionWhenError = (err) => {
        if (!err) {
          err = new Error("Election contract has not been validated within " + (timeoutMilliseconds / 1000) + "s")
        }
        done(err)
      }
      testHelper.waitAndRetryUntilFinished(waitMilliseconds, timeoutMilliseconds,
        actionToRetry, isResultOk, isFinished, actionWhenFinished, actionWhenError)

    }).timeout(timeoutMilliseconds)

    it("Should find Candidates with Address", done => {
      const actionToRetry = () => {
        return Election.findCandidatesByAddress(electionSCAddress)
      }
      const isResultOk = result => {
        CHAI.expect(result).not.be.null
      }
      const isFinished = result => {
        return result.length > 0
      }
      const actionWhenFinished = result => {
        CHAI.assert.deepEqual(result, candidates)
        done()
      }
      const actionWhenError = (err) => {
        if (!err) {
          err = new Error("Election ballot has not been validated within " + (timeoutMilliseconds / 1000) + "s")
        }
        done(err)
      }
      testHelper.waitAndRetryUntilFinished(waitMilliseconds, timeoutMilliseconds,
        actionToRetry, isResultOk, isFinished, actionWhenFinished, actionWhenError)

    }).timeout(timeoutMilliseconds)
  })

  describe("STEP ADD BALLOT", () => {

    const ballot = "FirstCandidate"
    const voterID = "0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF27"

    it("Should add ballot into Election smart contract", done => {
      Election.addBallot(electionSCAddress, voterID, ballot)
        .catch(done)
        .then(transactionHash => {
          CHAI.expect(transactionHash).not.be.null
          electionTransactionHash = transactionHash
          done()
        })
    }).timeout(timeoutMilliseconds)

    it("Should find Receipt ok after ballot added", done => {
      const actionToRetry = () => {
        return Election.findReceiptByTransactionHash(electionTransactionHash)
      }
      const isResultOk = result => {
        CHAI.expect(result).not.be.null
      }
      const isFinished = result => {
        return Election.isBallotAdded(result)
      }
      const actionWhenFinished = result => {
        electionSCAddress = result.to
        CHAI.expect(electionSCAddress).not.be.null
        done()
      }
      const actionWhenError = (err) => {
        if (!err) {
          err = new Error("Ballot has not been validated within " + (timeoutMilliseconds / 1000) + "s")
        }
        done(err)
      }
      testHelper.waitAndRetryUntilFinished(waitMilliseconds, timeoutMilliseconds,
        actionToRetry, isResultOk, isFinished, actionWhenFinished, actionWhenError)

    }).timeout(timeoutMilliseconds)

    it("Should find Ballot by VoterID", done => {
      Election.findBallotByAddressAndVoterID(electionSCAddress, voterID)
        .catch(done)
        .then(result => {
          CHAI.expect(ballot).equal(result)
          done()
        })
    }).timeout(timeoutMilliseconds)
  })

  describe("STEP RESULT", () => {

    const candidates = ["FirstCandidate", "challenger", "OtherCandidate"]
    const voterID = [
      "0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF26",
      "0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF34",
      "0xdfA1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF27",
      "0xC7A1A7B609087DA7BB4B6A4C9312D4606080C6DE342794AC944B5F9DF539AF27",
      "0xC7A1A7B609087DA7BB4B6A4C9312D4606080C6DE342794AC944B5F9DF539AF99"
    ]

    it("Should get the election result ", async () => {

      await Election.addBallot(electionSCAddress, voterID[0], candidates[0])
      await Election.addBallot(electionSCAddress, voterID[1], candidates[0])
      await Election.addBallot(electionSCAddress, voterID[2], candidates[1])
      await Election.addBallot(electionSCAddress, voterID[3], candidates[0])
      await Election.addBallot(electionSCAddress, voterID[4], candidates[1])

      const electionResult = await Election.findElectionResult(electionSCAddress)

      CHAI.assert.equal(electionResult[candidates[0]], 4)
      CHAI.assert.equal(electionResult[candidates[1]], 2)
      CHAI.assert.equal(electionResult[candidates[2]], 0)
    }).timeout(timeoutMilliseconds)
  })
})
