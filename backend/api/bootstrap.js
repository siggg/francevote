require("@babel/register")({
  extends: "./.babelrc",
  ignore: [/node_modules/],
})

let server, startServer

async function getServerObjects () {
  const data = await require("./src/server")
  server = data.server
  startServer = data.startServer
}

getServerObjects()
  .then(() => {
    startServer()
  })

