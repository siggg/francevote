module.exports = {
  "parser": "babel-eslint",

  "env": {
    "node": true,
    "mocha": true
  },

  "extends": [
    "standard"
  ],

  "plugins": [
    "babel", "chai-friendly",
  ],

  "rules": {
    "indent": ["error", 2],
    "key-spacing": 0,
    "max-len": [2, 350, 2],
    "object-curly-spacing": [2, "always"],
    "padded-blocks": 0,
    "quotes": [2, "double", "avoid-escape"],
    "template-curly-spacing": ["error", "always"],
    "comma-dangle": 0,
    "no-multiple-empty-lines": 0,
    "no-unused-vars": ["error", { "argsIgnorePattern": "^_" }],
    "no-return-await": 0,
    "no-unused-expressions": 0,
  }
}
