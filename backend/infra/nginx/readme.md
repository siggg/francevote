# NGINX - PROXY SERVER

## Installing Nginx on AWS ec2 instance with Ubuntu

### Install

```sh
sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install nginx -y
```

### Check status of Nginx and start it

```sh
sudo systemctl status nginx    # To check the status of nginx
sudo systemctl start nginx     # To start nginx
```

### Make sure that Nginx will run on system startup

```sh
sudo systemctl enable nginx
```

Nginx should be up and running

## Create a Self-Signed SSL Certificate for Nginx in Ubuntu 18.04

### Step 1 – Creating the SSL Certificate

#### Create a self-signed key and certificate pair with OpenSSL

```sh
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
```

#### Create a strong Diffie-Hellman group, which is used in negotiating Perfect Forward Secrecy with clients

```sh
sudo openssl dhparam -out /etc/nginx/dhparam.pem 4096
```

### Step 2 – Configuring Nginx to Use SSL

#### Creating a Configuration Snippet Pointing to the SSL Key and Certificate

* Create a new Nginx configuration snippet in the /etc/nginx/snippets directory

```sh
sudo vi /etc/nginx/snippets/self-signed.conf
```

Add

```sh
ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
```

#### Creating a Configuration Snippet with Strong Encryption Settings

```sh
sudo vi /etc/nginx/snippets/ssl-params.conf
```

Add

```sh
ssl_protocols TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_dhparam /etc/nginx/dhparam.pem;
ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
ssl_session_timeout  10m;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off; # Requires nginx >= 1.5.9
ssl_stapling on; # Requires nginx >= 1.3.7
ssl_stapling_verify on; # Requires nginx => 1.3.7
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
# Disable strict transport security for now. You can uncomment the following
# line if you understand the implications.
# add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
```

#### Change file sites-available/default

```sh
sudo rm /etc/nginx/sites-available/default
sudo vi /etc/nginx/sites-available/default
```

Add

```sh
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        server_name _;

        return 301 https://$host$request_uri;
}

server {
        # SSL configuration
        #
        listen 443 ssl;
        listen [::]:443 ssl;
        include snippets/self-signed.conf;
        include snippets/ssl-params.conf;

        server_name _;

        location / {
            proxy_pass                      http://localhost:8080;
            proxy_http_version              1.1;
            proxy_set_header                Upgrade $http_upgrade;
            proxy_set_header                Connection 'upgrade';
            proxy_set_header                Host $host;
            proxy_cache_bypass              $http_upgrade;
        }
}
```

### Step 3 – Adjusting the Firewall

```sh
sudo ufw app list
sudo ufw allow 'Nginx Full'
sudo ufw status
```

### Step 4 – Enabling the Changes in Nginx

```sh
sudo nginx -t
sudo systemctl restart nginx
```







### Generate the Certificate Signing Request (CSR)

```sh
sudo openssl req -new -key /etc/ssl/private/cd92.api.key -out /etc/ssl/private/cd92.api.csr
```

Press [ENTER] (blank) for “A challenge password”

#### Make sure the files are readable by root only

```sh
sudo chmod 0400 /etc/ssl/private/cd92.api.*
```


## Add HTTPS redirection

### Change file sites-available/default

```sh
sudo rm /etc/nginx/sites-available/default
sudo vi /etc/nginx/sites-available/default
```

* Add

```sh
server {
    listen 80;
    server_name _;
    location / {
        proxy_pass          http://localhost:8080;
        proxy_read_timeout  90;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
     }
}
```



