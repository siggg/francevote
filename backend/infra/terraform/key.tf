resource "aws_key_pair" "tick_stack" {
  key_name = "tick_stack"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
}
