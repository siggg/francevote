resource "aws_instance" "tick_stack" {
  ami = "${var.AMI}"
  instance_type = "${var.INSTANCE_TYPE}"

  tags {
    Name = "${var.hostname}"
  }

  # the VPC subnet
  subnet_id = "${aws_subnet.main-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.tick_sg.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.tick_stack.key_name}"

  provisioner "local-exec" {
    command = "echo ${aws_instance.tick_stack.private_ip} >> private_ips.txt"
    command = "echo ${aws_instance.tick_stack.public_ip} >> public_ips.txt"
  }
}
