output "instance_public_dns" {
  value = "${aws_instance.tick_stack.public_dns}"
}

output "ips" {
    value = "${aws_instance.tick_stack.public_ip}"
}
