variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "AWS_REGION" {
  default = "eu-west-3"
}

variable "INSTANCE_TYPE" {
  default = "t2.micro"
}

variable "AMI" {
  description = "Ubuntu Server 16.04 LTS"
  default = "ami-6775c31a"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "tick_stack.pub"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "tick_stack"
}

variable "hostname" {
  description = "EC2 hostname"
  default = "tick_stack"
}

variable "sg_name" {
  description = "Security Group name"
  default = "tick_sg"
}

variable "sg_description" {
  description = "SG description"
  default = "Allow InfluxDB, Chronograf & SSH access"
}

variable "s3_bucket" {
  default = "terraform-state-euwest2"
  description = "S3 bucket where remote state and Jenkins data will be stored."
}
