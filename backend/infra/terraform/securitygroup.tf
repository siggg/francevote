resource "aws_security_group" "tick_sg" {
  name = "tick_sg"
  vpc_id = "${aws_vpc.main.id}"
  description = "Security Group name"

  tags {
    Name = "tick_sg"
  }

  ingress {
    from_port   = 8083
    to_port     = 8083
    protocol    = "tcp"
    description = "InfluxDB admin dashboard"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8086
    to_port     = 8086
    protocol    = "tcp"
    description = "InfluxDB API"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8888
    to_port     = 8888
    protocol    = "tcp"
    description = "Chronograf Dashboard"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH access"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    description = "Allow all outbound traffic"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
