# TICK Stack

<p align="center">
  <img src="logo.png" width="70%"/>
</p>

---

# How to use

## Terraform
```sh
cd terraform
```
* Make private key (and public key)
```sh
ssh-keygen -f tick_stack
```
* Update terraform.tfvars with your AWS credentials
* Install AWS plugin
```sh
terraform init
```
* Create the AWS resources
```sh
terraform plan
terraform apply
```

---

## Ansible & Docker
```sh
cd ansible
```
* Update the `inventory` file with your instance DNS name
  * for example
```txt
[servers]
ec2–52–206–156–244.compute-1.amazonaws.com
```
* Then, install the Ansible custom role
```sh
ansible-galaxy install geerlingguy.pip
ansible-galaxy install andrewrothstein.docker-py
ansible-galaxy install mlabouardy.tick
```
* Execute the Ansible Playbook
```sh
ansible-playbook playbook.yml -i inventory —-private-key=./tick_stack
```

---

## Configure Chronograf (Dashboard, Kapacitor, Slack and Alert)
cf. [Continuous Monitoring With TICK Stack](https://dzone.com/articles/continuous-monitoring-with-tick-stack)
* Test Slack Alert message by stressing the CPU
  * SSH connection
  ```sh
  chmod 400 tick_stack
  ssh 35.180.99.219 -l ubuntu -i tick_stack

  sudo apt install nodejs-legacy
  sudo apt install npm
  sudo apt install python-minimal

  ls -al
  cd cd92/poc_vote

  git clone -b development --single-branch git@bitbucket.org:cd92_blockchain/poc_vote.git
  ```
* Lancement de l'API
```sh
  screen -ls # List of running Screen sessions
  screen -S api_session npm start
```
  * Pour se détacher de la session du screen :
    * Saisir la suite de touche clavier suivante : [CTRL]+[a] suivi de [d]
    * Ou fermer le terminal et/ou ouvrir un autre terminal
  * Pour se rattacher à la session du screen
  ```sh
    screen -r api_session
  ```


