pragma solidity ^0.5.0;

contract Election {

    bool public isInitiated = false;

    address OwnerAddress;
    bytes32[] public candidates;
    bytes32[] votersID;
    mapping(bytes32 => bytes32) public candidateByVoterID;
    mapping(bytes32 => uint) public counting;

    event CandidatesAdded(bool added);
    event BallotAdded(bool added);

    constructor () public {
        OwnerAddress = msg.sender;
        isInitiated = true;
    }

    function createCandidateList(bytes32[] memory _candidates) public onlyFromOwner {
        require(candidates.length == 0);
        candidates = _candidates;
        emit CandidatesAdded(true);
    }

    function vote(bytes32 voterID, bytes32 voterChoice) public onlyFromOwner {
        require(candidateByVoterID[voterID] == 0x0);
        votersID.push(voterID);
        addBallot(voterID, voterChoice);
        emit BallotAdded(true);
    }

    function addBallot(bytes32 voterID, bytes32 _ballot) private {
        candidateByVoterID[voterID] = _ballot;
        counting[_ballot] ++;
    }

    function getCandidates() public view returns (bytes32[] memory){
        return candidates;
    }

    function getVotersID() public view returns (bytes32[] memory){
        return votersID;
    }

    modifier onlyFromOwner {
        require(msg.sender == OwnerAddress);
        _;
    }
}
