module.exports = {
  "parser": "babel-eslint",

  "env": {
    "browser": true,
    "node": true,
    "mocha": true
  },

  "globals": {
    "assert": true,
    "contract": true,
    "artifacts": true
  },

  "rules": {
    "semi": ["error", "always"],
    "quotes": ["error", "double"],
    "indent": ["error", 2],
  },
}
