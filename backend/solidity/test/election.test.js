const Election = artifacts.require("Election");

const GASPRICE = web3.utils.toWei("25", "gwei");

contract("Election", accounts => {

  describe("Initialisation Election", () => {
    it("should create election", async () => {
      let emptyArray = [];
      let instance = await Election.new({from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await instance.createCandidateList(emptyArray, {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      let election = instance.contract.address;
      assert.isNotNull(election, "election is Null");
    });

    it("should create electionBBBBBB", (done) => {
      Election.new({from: accounts[0], gas: 2000000, gasPrice: GASPRICE})
        .once("transactionHash", (transactionHash) => {
          assert.isNotNull(transactionHash);
          done()
        });
    });

    it("should create election with only one candidate", async () => {
      const candidates = ["FirstCandidate"];
      const candidatesBytes32 = candidates.map(item => web3.utils.asciiToHex(item));
      let instance = await Election.new({from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await instance.createCandidateList(candidatesBytes32, {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      let candidateName = await instance.getCandidates.call();
      assert.equal(web3.utils.hexToUtf8(candidateName[0]), candidates[0]);
    });

    it("should create election with multiples candidates", async () => {
      let candidates = ["FirstCandidate", "challenger", "OtherCandidate"];
      const candidatesBytes32 = candidates.map(item => web3.utils.asciiToHex(item));
      let instance = await Election.new({from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await instance.createCandidateList(candidatesBytes32, {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      let candidatesNameBytes32 = await instance.getCandidates.call();
      const candidatesName = candidatesNameBytes32.map(item => web3.utils.hexToUtf8(item));
      assert.deepEqual(candidatesName, candidates);
    });

    it("should create election with multiples candidates just one time", (done) => {
      let candidates = ["FirstCandidate", "challenger", "OtherCandidate"];
      let candidates2 = ["FirstCandidate2", "challenger2", "OtherCandidate2"];
      const candidatesBytes32 = candidates.map(item => web3.utils.asciiToHex(item));
      const candidatesBytes32two = candidates2.map(item => web3.utils.asciiToHex(item));
      Election.new({from: accounts[0], gas: 2000000, gasPrice: GASPRICE})
        .then(instance => {
          instance.createCandidateList(candidatesBytes32, {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
          return instance;
        })
        .then(instance => {
          instance.createCandidateList(candidatesBytes32two, {from: accounts[0], gas: 2000000, gasPrice: GASPRICE})
            .catch(err => {
              assert.isOk(err);
              done();
            })
            .then(receipt =>
              assert.isUndefined(receipt, "User should not have be able to create candidate list if it is not CD92"));
        });
    });
  });

  describe("Voter", () => {
    let voterID;
    let candidates = [];
    let election;
    let candidatesBytes32 = [];

    beforeEach("Deploy an election contract implemented with candidates and voter", async () => {
      voterID = "0xC7A1A7B609087DA7244B6A4C9312D4606080C6DE342794AC944B5F9DF539AF27";
      candidates = ["FirstCandidate", "challenger", "OtherCandidate"];
      candidatesBytes32 = candidates.map(item => web3.utils.asciiToHex(item));
      election = await Election.new({from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.createCandidateList(candidatesBytes32, {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      return election;
    });

    it("should create a new ballot", async () => {
      await election.vote(voterID, candidatesBytes32[0], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      const voteVerificationByte32 = await election.candidateByVoterID.call(voterID);
      const voteVerification = web3.utils.hexToUtf8(voteVerificationByte32);
      assert.equal(voteVerification, candidates[0]);
    });

    it("should vote only one time", (done) => {
      election.vote(voterID, candidatesBytes32[0], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE})
        .then(() => {
          election.vote(voterID, candidatesBytes32[0], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE})
            .catch(err => {
              assert.isOk(err);
              done();
            })
            .then(receipt =>
              assert.isUndefined(receipt, "User should vote only one time"));
        });
    });

    it("should authorize only by the Owner to create a ballot", (done) => {
      election.vote(voterID, candidatesBytes32[1], {from: accounts[1], gas: 2000000, gasPrice: GASPRICE})
        .catch(err => {
          assert.isOk(err);
          done();
        })
        .then(receipt =>
          assert.isUndefined(receipt, "User should not have be able to vote if it is not CD92"));
    });
  });

  describe("Multi Voters Election", () => {
    let candidates = [];
    let candidatesBytes32 = [];
    let election;

    beforeEach("Deploy an election contract with candidates", async () => {
      candidates = ["FirstCandidate", "challenger", "OtherCandidate"];
      candidatesBytes32 = candidates.map(item => web3.utils.asciiToHex(item));
      election = await Election.new({from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.createCandidateList(candidatesBytes32, {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      return election;
    });

    it("should find all voters", async () => {
      let voterID = "0xcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc";
      let voterID2 = "0xbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
      let voterID3 = "0x3333333333333333333333333333333333333333333333333333333333333333";
      await election.vote(voterID, candidatesBytes32[0], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.vote(voterID2, candidatesBytes32[1], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.vote(voterID3, candidatesBytes32[0], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      let listOfVoterID = await election.getVotersID.call();
      assert.equal(listOfVoterID[0], voterID);
      assert.equal(listOfVoterID[1], voterID2);
      assert.equal(listOfVoterID[2], voterID3);
    });
  });

  describe("Election Result", () => {
    let voterID = [
      "0xcccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc",
      "0xbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
      "0x3333333333333333333333333333333333333333333333333333333333333333",
      "0x1111111111111111111111111111111111111111111111111111111111111111",
      "0x3456543456787654456789876545678987654456787654567654765676567654",
      "0x3456543456787654456789876545678987654456787654567654765aaaaaaaaa"
    ];

    let candidates = [];
    let election;
    let candidatesBytes32 = [];

    before("Deploy an election contract implemented with candidates and voter", async () => {
      candidates = ["FirstCandidate", "challenger", "OtherCandidate"];
      candidatesBytes32 = candidates.map(item => web3.utils.asciiToHex(item));
      election = await Election.new({from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.createCandidateList(candidatesBytes32, {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      return election;
    });

    it("should perform the counting", async () => {
      await election.vote(voterID[0], candidatesBytes32[1], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.vote(voterID[1], candidatesBytes32[1], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.vote(voterID[2], candidatesBytes32[0], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.vote(voterID[3], candidatesBytes32[1], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.vote(voterID[4], candidatesBytes32[0], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      await election.vote(voterID[5], candidatesBytes32[1], {from: accounts[0], gas: 2000000, gasPrice: GASPRICE});
      let result = [];
      result[0] = await election.counting.call(candidatesBytes32[0], {
        from: accounts[0],
        gas: 2000000,
        gasPrice: GASPRICE
      });
      result[1] = await election.counting.call(candidatesBytes32[1], {
        from: accounts[0],
        gas: 2000000,
        gasPrice: GASPRICE
      });
      result[2] = await election.counting.call(candidatesBytes32[2], {
        from: accounts[0],
        gas: 2000000,
        gasPrice: GASPRICE
      });
      assert.equal(result[0], 2);
      assert.equal(result[1], 4);
      assert.equal(result[2], 0);
    });
  });
});
