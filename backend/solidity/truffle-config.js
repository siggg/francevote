module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
      gas: 0x1fffffffffffff, // Gas sent with each transaction (default: ~6700000)
      gasPrice: 30000000000  // 30 gwei (in wei) (default: 100 gwei)
    },
  },

  compilers: {
    solc: {
      optimizer: {
        enabled: true,
        runs: 200
      },
    }
  }
}
