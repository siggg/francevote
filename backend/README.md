# CD92 - PoC Blockchain - API - Vote
![logo](doc/api/img/CD92.png)

## TODO About API

## Ethereum versions used

### From [package.json](solidity/package.json) file

#### Truffle - Development framework for Ethereum
* Version: 5.0.1
* Language: javascript
* [Official documentation](https://truffleframework.com/docs/truffle/overview)
* [Official technical documentation](https://github.com/trufflesuite/truffle)

#### Web3 - Ethereum JavaScript API
* Version: 1.0.0-beta.37
* Language: javascript
* [Official documentation](https://web3js.readthedocs.io/en/1.0)
* [Github](https://github.com/ethereum/web3.js/tree/1.0)

#### Ethereum Smart contract
* Solidity - High-level language contract oriented
* solc version: 0.5.0
* [Official documentation](https://solidity.readthedocs.io/en/v0.5.0)
* [Other readme](doc/ethereum/tools.md#solidity---how-to-write-smart-contract)

#### Ethereum client node
* Geth - Official Go implementation of the Ethereum protocol
* Version: 1.8.22
* [Official documentation](https://geth.ethereum.org)
* [Github](https://github.com/ethereum/go-ethereum)

## Node JS versions used

### From [.nvmrc](api/.nvmrc) file
* node 8.11.3
* npm 6.5.0

----

## How to test?

### Testing Smart Contracts

#### Launch Ganache into another terminal

```sh
cd solidity
npm i
npm run ganache &
```

#### Run solidity tests

```sh
npm run compile
npm run test
```

### Testing API

#### Go into the directory

```sh
cd api
npm i
```

#### Make sure you have a ```api/.env``` file like

```sh
export JWT_SECRET=ThisIsMySecretAPI_KEY
...
```

#### Make sure Ganache is launching into another terminal, and run Mocha tests

```sh
npm run test-api
```

---

## [HAPI.JS Documentation](doc/api/hapijs.md)

## [API Swagger Documentation](./doc/api/swagger.md)

## [Mocha testing Documentation](doc/api/mocha.md)

## [GIT method and tools](doc/ci/git.md)

## [Ethereum tooling](doc/ethereum/tools.md)

## [Infrastructure tooling](doc/infra/tools.md)


