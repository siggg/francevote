// OCTO API backend
var backendURL = "https://35.180.99.219";
var apiURL = backendURL + "/";
var apiKey = "637E92E5742736C748DBB6678A6965AE7E4362505E9B8AC3B2CA3FCC156CC4A1";
var apiToken;
// Age of first election to be listed, unit = blocks
// On the Ropsten testnet : 2000000 blocks is about one year ago and one block seems to be about 10 secondes long
var ageOfFirstElection = 400;
// Etherscan API
// Should we use the Etherscan API instead of the Octo API whenever possible ?
var etherscan = false;
var etherscanApiKey = "PB8BWBNFYSC7KRCZC8ESWHMS79AD996W9Q";
// Address of the blockchain account creating elections
var creatorAddress = "0x5B2398DfDcB3BB0eed76Ec0e2D6d925a93fBF25e";
var web3EthAbi = require('web3-eth-abi');
// Cookies helper
var Cookies = require('js-cookie');


$(function() {

$('.loading').html('(en cours de chargement, patientez quelques secondes SVP)');

// API authentication

function postAPIAuthentication() {
  // authenticates this code against the backend API using the apiKey
  // and set an authorization token for use in further requests
  return $.ajax({
    type: "POST",
    crossDomain: true,
    beforeSend: function(request) {
      // request.setRequestHeader("Origin", "http://siggg.gitlab.io");
    },
    url: apiURL + "auth",
    data: { "apiKey": apiKey },
    dataType: "json",
  }).done( function (json) {
    apiToken = json.token;
    console.log("postAPIAuthentication success");
  }).fail( function (xhr, statusText) {
    console.log("postAPIAuthentication error: ", statusText);
  });
}

var apiAuthRequest = postAPIAuthentication();

function setHeader(jqXHR) {
  // set the "Authorization" request header using the token given by the API
  jqXHR.setRequestHeader("Authorization", "Bearer " + apiToken);
  jqXHR.setRequestHeader("Accept", "*/*");
}

// elections

function getBlockNumber() {
  // retrieve the current block number on the blockchain
  var thisURL = apiURL + "blocknumber";
  return $.ajax({
    type: "GET",
    dataType: "json",
    beforeSend: setHeader,
    url: thisURL
  }).done( function( json ) {
    console.log("getBlockNumber success");
    console.log("getBlockNumber response: ", json);
  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = jqxhr.responseText + ", " + textStatus + ", " + error;
    console.log("getElections error: ", errorMessage);
  });
}

function getElections( electionTransaction ) {
  // retrieve elections
  // optionnally using the hash of a transaction creating an election
  if ( true == true ) { // use etherscan, always because parsing blocks is too slow.
    var etherscanURL = "https://api-ropsten.etherscan.io/api?module=account&action=txlist";
    etherscanURL += "&address=" + creatorAddress;
    etherscanURL += "&startblock=0&endblock=99999999";
    etherscanURL += "&apikey=" + etherscanApiKey;
    return $.ajax({
      type: "GET",
      dataType: "json",
      url: etherscanURL
    }).done( function( json ) {
      console.log("etherscan call success");
      console.log("transactions found via etherscan: ", json.result.length);
    }).fail( function( jqxhr, textStatus, error ) {
      var errorMessage = jqxhr.responseText + ", " + textStatus + ", " + error; 
      console.log("etherscan call error: ", errorMessage);
    });
  } else {
    var blockNumberXHR = getBlockNumber();
    return blockNumberXHR.then( function(json) {
      var currentBlockNumber = json;
      var thisURL = apiURL + "elections";
      if ( electionTransaction != undefined ) {
        thisURL += "/" + electionTransaction;
      } else {
        var oldestElectionBlockNumber = currentBlockNumber - ageOfFirstElection;
        thisURL += "?startBlockNumber=" + oldestElectionBlockNumber;
      }
      return $.ajax({
        type: "GET",
        dataType: "json",
        beforeSend: setHeader,
        url: thisURL
      }).done( function( json ) {
        console.log("getElections success");
        json.listOfElectionAddress.forEach( function(electionAddress) {
          console.log("getElections found an election at: ", electionAddress);
        });
        console.log("smartContractAddress: ", json.smartContractAddress);
        console.log("status: ", json.status);
      }).fail( function( jqxhr, textStatus, error ) {
        var errorMessage = jqxhr.responseText + ", " + textStatus + ", " + error;
        console.log("getElections error: ", errorMessage);
      });
    });
  }
}

function postElections() {
  // create a new election into the ethereum blockchain
  // the location HTTP response header provides the URL of the transaction creating the new election
  return $.ajax({
    type: "POST",
    beforeSend: setHeader,
    url: apiURL + "elections"
  }).done( function( json, textStatus, jqxhr ) {
    console.log("postElections - election being created with HTTP location: ", jqxhr.getResponseHeader("location"));
    console.log("json returned: ", json);
    console.log("status: ", textStatus);
  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = textStatus + ", " + error;
    console.log("postElections error:", errorMessage, jqxhr);
  });
}

function waitForTransactionResult(transactionHash, callback) {
  // checks whether the transaction given as parameter has been mined on the blockchain
  // then callback
  // let's ask etherscan about the status of this transaction
  var etherscanURL = "https://api-ropsten.etherscan.io/api?module=proxy&action=eth_getTransactionReceipt";
  etherscanURL += "&txhash=" + transactionHash;
  etherscanURL += "&startblock=0&endblock=99999999";
  etherscanURL += "&apikey=" + etherscanApiKey;
  return $.ajax({
    type: "GET",
    dataType: "json",
    url: etherscanURL
  }).done( function( json ) {
    console.log("etherscan call success while checking for transaction result");
    // if transaction is not ready, then retry after some delay (recursively, for ever until tx success)
    var delayInSeconds = 5;
    if ( json.result == null ) {
      console.log("no transaction receipt yet, let's retry in " + delayInSeconds + " seconds");
      setTimeout(function(){
        waitForTransactionResult(transactionHash, callback);
      }, delayInSeconds * 1000);
    } else {
      callback(json);
    }
  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = jqxhr.responseText + ", " + textStatus + ", " + error;
    console.log("etherscan call error while checking for transaction result: ", errorMessage, jqxhr);
  });
}

if ($('#elections_list').length > 0) {
  apiAuthRequest
    .done(function() {
      var electionsXHR = getElections();
      electionsXHR.done(function(json) {
        var electionAddresses = [];
        if ( true == true ) { // use etherscan indeed
          json.result.forEach(function(tx) {
            if ( tx.contractAddress != "" && tx.isError == "0" ) {
              electionAddresses.push(tx.contractAddress);
            }
          });
          electionAddresses.reverse(); // newest first
        } else {
          electionAddresses = json.listOfElectionAddress;
        }
        electionAddresses.forEach(function(electionAddress) {
          var li = '<li><a href="/francevote/election/?electionAddress=';
          li += electionAddress;
          li += '">';
          li += electionAddress.substring(0,12);
          li += '(...)</a></li>';
          $('#elections_list').append(li);
        });
        $('.loading').remove();
      });
    // Do we have to create a new election ?
    var newElection = (new URL(document.location)).searchParams.get("newElection");
    if ( newElection != undefined ) {
      console.log("election creation with these candidates: ", newElection);
      var newElectionXHR = postElections();
      newElectionXHR.done(function(json) {
        console.log("election transaction was sent to the blockchain");
        var newElectionTransaction = newElectionXHR.getResponseHeader("location");
        newElectionTransaction = newElectionTransaction.substring("/elections?tx=".length,newElectionTransaction.length);
        var electionPending = '<li class="electionPending">';
        electionPending += 'Nouveau scrutin en cours de création sur la blockchain ';
        electionPending += '(transaction ' + newElectionTransaction + '). ';
        electionPending += 'Dans quelques dizaines de secondes, le scrutin devrait être ';
        electionPending += 'suffisamment validé par les dizaines de milliers d\'ordinateurs ';
        electionPending += 'qui vérifient en permanence cette blockchain. ';
        electionPending += 'Nous inscrirons alors les candidats sur ce scrutin. ';
        electionPending += 'Ne quittez pas cette page tant que ce texte est visible ';
        electionPending += 'ou sinon les candidats risquent de ne pas être inscrits.';
        electionPending += '</li>';
        $('#elections_list').prepend(electionPending);
        function processElectionCreationReceipt(json) {
          console.log("transaction receipt obtained for creating an election"); // json.result
          if (json.result.status == 1) {
            var newElectionAddress = json.result.contractAddress;
            console.log("election succesfully created on the blockchain, with address: ", newElectionAddress);
            newElection = newElection.replace(/\r/gi,"");
            var candidates = newElection.split("\n");
            // remove empty candidates
            candidates = candidates.filter(function(value, index, arr){
              return value != "";
            });
            var addCandidatesXHR = postElectionCandidates(newElectionAddress, candidates);
            addCandidatesXHR.done(function(json) {
              console.log("candidates transaction was sent to the blockchain");
              var candidatesAdditionTransaction = addCandidatesXHR.getResponseHeader("location");
              var prefixExample = "/elections/0x735bb67dcc7253e7360b15b2fbc415b4781d1766/candidates/";
              var prefixLength = prefixExample.length;
              candidatesAdditionTransaction = candidatesAdditionTransaction.substring(prefixLength, candidatesAdditionTransaction.length);
              var candidatesPending = '<li class="electionPending">';
              candidatesPending += "Scrutin créé sur la blockchain. ";
              candidatesPending += "Ajout des candidats en cours. ";
              candidatesPending += "Dans quelques dizaines de secondes, le scrutin devrait être ";
              candidatesPending += "suffisamment validé par les dizaines de milliers d'ordinateurs ";
              candidatesPending += "qui vérifient en permanence cette blockchain. ";
              candidatesPending += "Il sera alors ouvert au vote.</li> ";
              $('.electionPending').replaceWith(candidatesPending);
              function processCandidatesAdditionReceipt(json) {
                console.log("transaction receipt obtained for adding candidates: ", json.result);
                if (json.result.status == 1) {
                  console.log("candidates successfully added on the blockchain");
                  var successMsg = '<li class="electionPending">';
                  successMsg += 'Scrutin créé, avec ses candidats, sur la blockchain. ';
                  successMsg += 'Vous pouvez désormais <a href="/francevote/election/?electionAddress=' ;
                  successMsg += newElectionAddress + '">voter sur le scrutin ';
                  successMsg += newElectionAddress.substring(0,10) + '(...)</a></li>';
                  $('.electionPending').replaceWith(successMsg);
                } else {
                  console.log("failed while adding candidates on the blockchain, transaction ran out of gas or was reverted by contract");
                  var failureMsg = '<li>Erreur : les candidats n\'ont pas pu être ajoutés sur la blockchain. ';
                  failureMsg += 'Veuillez signaler cette erreur aux responsables de ce site. </li>';
                  $('.electionPending').replaceWith(failureMsg);
                }
              }
              waitForTransactionResult(candidatesAdditionTransaction, processCandidatesAdditionReceipt);
            });
          } else {
            console.log("failed while creating election on the blockchain, transaction ran out of gas ?");
            var failureMsg = '<li>Erreur : le scrutin n\'a pas pu être créé sur la blockchain. ';
            failureMsg += 'Veuillez signaler cette erreur aux responsables de ce site. </li>';
            $('.electionPending').replaceWith(failureMsg);
          }
       }
       waitForTransactionResult(newElectionTransaction, processElectionCreationReceipt);
      });
    }
  });
}

// election

function postElectionCandidates(electionAddress, candidates) {
  // add a new candidates list into smart contract election.
  var thisURL = apiURL + "elections";
  thisURL += "/" + electionAddress;
  thisURL += "/candidates";
  var json = { candidates: candidates }; // ["AAAA", "BBB", "CCC"] };
  console.log("postElectionCandidats with json and stringified: ", json, JSON.stringify(json));
  return $.ajax({
    type: "POST",
    dataType: "json",
    beforeSend: setHeader,
    url: thisURL,
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify(json)
  }).done( function( json, textStatus, xhr ) {
    console.log("postElectionCandidates sent this transaction: ", xhr.getResponseHeader("location"));
    console.log("  as stated here: ", json);
  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = textStatus + ", " + error;
    console.log("postElectionCandidates error", errorMessage);
  });
} 

function getElectionCandidates(electionAddress, candidatesTransaction) {
  // retrieve election candidates
  // optionnally including candidates being set by given transaction
  if ( etherscan == true ) {
    // In order to read the result of the getCandidates() method from the contract
    // we have to calculate the Keccak-256 hash of the string "getCandidates()" and
    // provide the first 8 characters of this hash and prepend them with 0x
    // Here is the result "0x06a49fce".
    var etherscanURL = "https://api-ropsten.etherscan.io/api?module=proxy&action=eth_call";
    etherscanURL += "&to=" + electionAddress;
    etherscanURL += "&data=0x06a49fce";
    etherscanURL += "&apikey=" + etherscanApiKey;
    console.log("etherscanURL for getElectionCandidates: ", etherscanURL);
    return $.ajax({
      type: "GET",
      dataType: "json",
      url: etherscanURL
    }).done( function( json ) {
      console.log("etherscan call success: ", json);
      var candidates = web3EthAbi.AbiCoder().decodeParameter("bytes32[]", json.result);
      console.log("candidates for this election: ", candidates);
      json.candidates = candidates;
    }).fail( function( jqxhr, textStatus, error ) {
      var errorMessage = jqxhr.responseText + ", " + textStatus + ", " + error;
      console.log("etherscan call error: ", errorMessage);
    });
  } else {
    var thisURL = apiURL + "elections";
    thisURL += "/" + electionAddress;
    thisURL += "/candidates";
    if (candidatesTransaction != undefined) {
      thisURL += "/" + candidatesTransaction;
    }
    return $.ajax({
      type: "GET",
      dataType: "json",
      beforeSend: setHeader,
      url: thisURL
    }).done( function( json ) {
      console.log("getElectionCandidates");
      console.log("candidates: ", json.candidates);
      console.log("status: ", json.status);
    }).fail( function( jqxhr, textStatus, error ) {
      var errorMessage = textStatus + ", " + error;
      console.log("getElectionCandidatesFromTransaction error", errorMessage);
    });
  }
}

function getElectionResults(electionAddress) {
  // retrieve election results
  var thisURL = apiURL + "elections";
  thisURL += "/" + electionAddress;
  thisURL += "/results";
  return $.ajax({
    type: "GET",
    dataType: "json",
    beforeSend: setHeader,
    url: thisURL
  }).done( function( json ) {
    console.log("getElectionResults returned electionResults: ", json.electionResults);
  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = textStatus + ", " + error;
    console.log("getElectionResults error", errorMessage);
  });
}


if ($('.election_name').length >0) {
  var electionAddress = (new URL(document.location)).searchParams.get("electionAddress");
  /*
  if ( electionAddress == undefined ) {
    electionAddress = "0x56Bb85D60e893e66d4B679a3084530a4CcBf2005";
  } */
  console.log("electionAddress is this one: ", electionAddress);
  Cookies.set("electionAddress", electionAddress);
  $("a[href*='voter/?electionAddress=']").attr("href","../voter/?electionAddress=" + electionAddress);
  apiAuthRequest
    .done(function() { 
      var electionXHR = getElectionResults(electionAddress);
      electionXHR
        .done(function(json) {
          var electionResults = json.electionResults;
          console.log("success getting election results: ", electionResults);
          Object.keys(electionResults).forEach(function(candidate) {
            var div = '<div>';
            div += '<input type="radio" name="ballot" id="';
            div += candidate;
            div += '" value ="';
            div += candidate;
            div += '">';
            div += '&nbsp;<label for="';
            div += candidate;
            div += '">';
            div += candidate;
            div += ' (' + electionResults[candidate].toString() + ' voix)';
            div += '</label><br /></div>';
            $('#election_fieldset').prepend(div);
          });
          $('#electionAddress').attr("value",electionAddress);
          $('.loading').remove();
        }).fail(function( jqxhr, textStatus, error) {
          console.log("error while getting election candidates: ", textStatus, error);
        });
    });
}

// voter

function getVoterBallots(electionAddress, voterID, ballotTransaction) {
  // retrieve voter ballots
  // optionnally using a transaction hash
  if ( etherscan == true ) {
    // In order to read the result of the candidateByVoterID(bytes32 input) method from the contract
    // we have to calculate the Keccak-256 hash of the string "candidateByVoterID(bytes32)" and
    // provide the first 8 characters of this hash and prepend them with 0x
    // Here is the result "0x0ca304dc".
    var etherscanURL = "https://api-ropsten.etherscan.io/api?module=proxy&action=eth_call";
    etherscanURL += "&to=" + electionAddress;
    etherscanURL += "&data=0x0ca304dc";
    etherscanURL += "&apikey=" + etherscanApiKey;
    console.log("etherscanURL for getVoterBallots: ", etherscanURL);
    return $.ajax({
      type: "GET",
      dataType: "json",
      url: etherscanURL
    }).done( function( json ) {
      console.log("etherscan call success for getVoterBallots: ", json);
      var ballot = web3EthAbi.AbiCoder().decodeParameter("bytes32", json.result);
      console.log("ballot, for voterID, at electionAddress: ", ballot, voterID, electionAddress);
      json.ballot = ballot;
    }).fail( function( jqxhr, textStatus, error ) {
      var errorMessage = jqxhr.responseText + ", " + textStatus + ", " + error;
      console.log("etherscan call error, during getVoterBallots: ", errorMessage, jqxhr);
    });
  } else {
    var thisURL = apiURL + "elections";
    thisURL += "/" + electionAddress;
    thisURL += "/voterID";
    thisURL += "/" + voterID;
    thisURL += "/ballots";
    if ( ballotTransaction != undefined ) {
      thisURL += "/" + transactionHash;
    }
    return $.ajax({
      type: "GET",
      dataType: "json",
      beforeSend: setHeader,
      url: thisURL
    }).done( function( json ) {
      console.log("getVoterBallots got json with candidate name: ", json.ballot);
      console.log(" and status: ", json.status);
    }).fail( function( jqxhr, textStatus, error ) {
      var errorMessage = textStatus + ", " + error;
      console.log("getVoterBallots error", errorMessage, jqxhr);
    });
  }
}

function getVoterID(electionAddress, voterSub) {
  // retrieve voterID from FranceConnect sub
  var thisURL = apiURL + "elections";
  thisURL += "/" + electionAddress;
  thisURL += "/" + voterSub;
  thisURL += "/voterID";
  return $.ajax({
    type: "GET",
    dataType: "json",
    beforeSend: setHeader,
    url: thisURL
  }).done( function( json ) {
    console.log("getVoterID got voterID: ", json.voterID);
    console.log("and maybe status: ", json.status);
    $('.voter_ID').html(json.voterID);
  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = textStatus + ", " + error;
    console.log("getVoterID error", errorMessage);
  });
}

var authenticated = true;

function toggleAuthenticated( userSub ) {
  authenticated = !authenticated;
  if ( authenticated == false ) {
    $('.anonymous').show();
    $('.authenticated').hide();
  } else if ( authenticated == true ) {
    // Show election details for this voter (ballots, etc.)
    $('.anonymous').hide();
    $('.authenticated').show();
    console.log("FranceConnect sub of the user is: ", userSub);
    // do we have a state from before signin to restore ?
    var electionAddress = Cookies.get("electionAddress");
    Cookies.remove("electionAddress");
    console.log("restored electionAddress: ", electionAddress);
    if ( electionAddress == undefined ) {
      console.log("no electionAddress specified");
      // Dear user, pick an election please
      window.location.replace("/francevote/elections/");
    }  
    var candidate = Cookies.get("candidate");
    Cookies.remove("candidate");
    console.log("restored candidate on ballot: ", candidate);
    apiAuthRequest
      .done(function() {
        var voterIDXHR = getVoterID(electionAddress, userSub);
        voterIDXHR
          .done(function(json) {
            console.log("success getting voter ID from user sub: ", json.voterID);
            var voterID = json.voterID;
            // Post new ballot if any
            var postBallot = ( electionAddress != undefined && candidate != undefined && voterID != undefined );
            if ( postBallot == true ) {
              postBallot = electionAddress.length > 0 && candidate.length > 0 && voterID.length > 0;
            } 
            if ( postBallot == true ) {
              var voteXHR = postElectionBallot( electionAddress, candidate, voterID );
              voteXHR
                .done(function(json) {
                  console.log("success posting ballot transaction, with json result: ", json);
                  var voteTransaction = voteXHR.getResponseHeader("location");
                  var prefixExample = "/elections/0x2052fee6b604afafa32e96160247869ca17e9687/voterID/0x188966b6912edd9502631275196f14b8ccd3224c57c8a091fba02a05cc05699a/ballots/";
                  var prefixLength = prefixExample.length;
                  voteTransaction = voteTransaction.substring(prefixLength, voteTransaction.length);
                  var votePending = '<div class="votePending">';
                  votePending += "Nouveau bulletin de vote en cours d'enregistrement sur la blockchain. ";
                  votePending += "Dans quelques dizaines de secondes, le bulletin de vote devrait être ";
                  votePending += "suffisamment validé par les dizaines de milliers d'ordinateurs ";
                  votePending += "qui vérifient en permanence cette blockchain. ";
                  votePending += "Il sera alors pris en compte dans les résultats du scrutin.</div> ";
                  $('.votePending').replaceWith(votePending);
                  function processVoteReceipt(json) {
                    console.log("transaction receipt obtained for posting ballot: ", json.result);
                    if (json.result.status == 1) {
                      console.log("ballot successfully added on the blockchain");
                      var successMsg = '<div class="votePending">';
                      successMsg += 'Un nouveau bulletin de vote est désormais enregistré sur la blockchain, ';
                      successMsg += 'pour le candidat "<strong>';
                      successMsg += candidate;
                      successMsg += '</strong>". </div>';
                      $('.votePending').replaceWith(successMsg);
                    } else {
                      console.log("failed while posting ballot to the blockchain, transaction ran out of gas ?");
                      var failureMsg = '<div>Erreur : le nouveau bulletin de vote n\'a pas pu être enregistré sur la blockchain. ';
                      failureMsg += 'Peut-être avez-vous déjà voté sur ce scrutin ? cf. ci-dessous. ';
                      failureMsg += 'Sinon, veuillez signaler cet incident erreur aux responsables de ce site. </div>';
                      $('.votePending').replaceWith(failureMsg);
                    }
                  }
                  waitForTransactionResult(voteTransaction, processVoteReceipt);
                }).fail(function(jqxhr, textStatus, error) {
                  console.log("error while posting ballot: ", textStatus, error, jqxhr);
                });
            }
            // Read user ballot from pending transaction if any
            // Read user ballots from the blockchain
            var ballotsXHR = getVoterBallots(electionAddress, voterID);
            ballotsXHR
              .done(function(json) {
                console.log("success getting voterBallots as json: ", json);
                var li;
                if ( json.ballot.length > 0 ) {
                  li = '<li>Bulletin de vote enregistré pour le candidat : "';
                  li += json.ballot;
                  li += '"</li>';
                } else {
                  li = '<li>Aucun bulletin de vote enregistré pour l\'instant.</li>';
                }
                $('#voter_ballots').append(li);
              }).fail(function(jqxhr, textStatus, error) {
                console.log("error getting voterBallots: ", textStatus, error, jqxhr);
              });
          }).fail(function( jqxhr, textStatus, error) {
            console.log("error getting voterID from user sub: ", textStatus, error, jqxhr);
          });
        var a = '<a class="election_link" href="../election?electionAddress=';
        a += electionAddress;
        a += '">scrutin ';
        a += electionAddress;
        a += '</a>';
        $('.election_link').html(a);        
      });
  }
} 

toggleAuthenticated();

// FranceConnect, French national Open ID Connect governmental platform

// Backend URLs for FranceConnect :
// GET /signin
// FranceConnect, step 1. the user clicks on the FC button

if ( $('#s_identifier').length > 0 ) {
  // Store important URL parameters before they are lost through authentication
  // unless they are not present in the URL

  var urlParameters = (new URL(document.location)).searchParams;
  console.log("all parameters: ", urlParameters.toString());
  var persistentElectionAddress = urlParameters.get('electionAddress');
  console.log("persistentElectionAddress: ", persistentElectionAddress);
  if (persistentElectionAddress != null) {
    Cookies.set('electionAddress', persistentElectionAddress);
  } /* else {
    Cookies.remove('electionAddress');
    console.log("removed electionAddress from cookies");
  } */
  var candidate = urlParameters.get('ballot');
  console.log("candidate: ", candidate);
  if (candidate != null) {
    Cookies.set('candidate', candidate);
  } /* else {
    Cookies.remove('candidate');
    console.log("removed candidate from cookies");
  } */

  // Enable authentication with FranceConnect
  $("button[name = 's_identifier']").on("click", getIdentityProviderLoginRedirect);

}


function getIdentityProviderLoginRedirect() {
  // FranceConnect, step 2. javascript login request to the backend service provider

  // GET /signin redirects to the authorize endpoint at the identity provider
  var serviceProviderRedirectURL = apiURL + 'signin?toto=titi';

  // toggleAuthenticated("myVoterSub");
  return $.ajax({
    type: "GET",
    dataType: "json",
    beforeSend: setHeader,
    url: serviceProviderRedirectURL
  }).done( function( data ) {
    // FranceConnect, step 3. service provider responds with a 302 redirect to the authorize endpoint at the identity provider
    console.log("get serviceProviderRedirectURL success: ", data);
    authorizeEndpointURL = data.authorizedURL;

    // FranceConnect, step 4. javascript redirects the user to the authorize endpoint at the identity provider
    // should not be necessary since browser should immediately execute HTTP redirect
    window.location.replace(authorizeEndpointURL);

  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = textStatus + ", " + error;
    console.log("get serviceProviderRedirectURL error", errorMessage);
  });
}

function getAuthenticatedResource( authorizationCode ) {
  // FranceConnect, step 5. the user gets the authorize endpoint at the identity provider then logs in
  // FranceConnect, step 6. the identity provider responds with a 302 response to the callback URL at the static server
  // FranceConnect, step 7. the user gets the callback resource at the static server

  // GET /oidcCallback/{authorizationCode} returns the user's sub
  var serviceProviderCallbackURL = apiURL + 'oidcCallback/' + authorizationCode;
  return $.ajax({
    type: "GET",
    beforeSend: setHeader,
    url: serviceProviderCallbackURL
  }).done( function( data ) {
    // FranceConnect, step 8. javascript request to the callback endpoint at the service provider
    // FranceConnect, step 9. the service provider posts to the token endpoint at the identity provider
    // FranceConnect, step 10. the service provider gets the userinfo resource at the identity provider
    // FranceConnect, step 11. the service provider responds to the javascript request with a 302 redirect to the authenticated page at the static server
    console.log("get serviceProviderCallback success: ", data);

    // FranceConnect, step 12. the user gets the authenticated page at the static server
    toggleAuthenticated( data );
  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = textStatus + ", " + error;
    console.log("error while getting serviceProviderCallback", errorMessage);
  });
}

// Are we on a page with user authentication ?
if ($('.authenticated').length >0) {
  // Does the URL contain a FranceConnect authorization code ?
  var authorizationCode = (new URL(document.location)).searchParams.get("code");
  if ( authorizationCode != undefined ) {
    apiAuthRequest
    .done(function() {
      getAuthenticatedResource( authorizationCode );
    });
  }
}

// ballot

function postElectionBallot( electionAddress, candidateName, voterID ) {
  // add new ballot to the election
  // the location HTTP header provides the URL of the new ballot
  var thisURL = apiURL + "elections";
  thisURL += "/" + electionAddress;
  thisURL += "/ballots"; 
  console.log("post ballot in election, for candidate, by voter: ", electionAddress, candidateName, voterID);
  return $.ajax({
    type: "POST",
    dataType: "json",
    beforeSend: setHeader,
    url: thisURL,
    data: { "ballot": candidateName, "voterID": voterID }
  }).done( function( json, textStatus, xhr ) {
    console.log("postElectionBallot - ballot being created by transaction: ", xhr.getResponseHeader("location"));
    // console.log("candidateName on ballot: ", json.ballot);
    // console.log("voterID: ", json.voterID);
  }).fail( function( jqxhr, textStatus, error ) {
    var errorMessage = textStatus + ", " + error;
    console.log("getElections error", errorMessage);
  });
}

if ($('.ballot_ID').length >0) {
  var ballotAddress = (new URL(document.location)).searchParams.get("ballotAddress");
  console.log("ballotAddress is: ", ballotAddress);
  apiAuthRequest
    .done(function() {
      var ballotXHR = getBallotCandidate(ballotAddress);
      ballotXHR
        .done(function(json) {
          console.log("success getting ballot candidate, with result: ", json);
          $('.ballot_candidate').html(json.ballot_candidate);
        }).fail(function( jqxhr, textStatus, error) {
          console.log("error while getting ballot candidate: ", textStatus, error);
        });
    });
}

//

});
